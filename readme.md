# Geo Frontend Repo

## Quick Links

- [Redux Toolkit](https://redux-toolkit.js.org/tutorials/basic-tutorial)
- [React native maps](https://github.com/react-native-community/react-native-maps)
- [React native elements](https://react-native-elements.github.io/react-native-elements/docs/overview)
- [React navigation](https://reactnavigation.org/docs/getting-started/)
- [Font Awesome (Icons)](https://fontawesome.com/icons?d=gallery)
- [Google Maps styling wizard](https://mapstyle.withgoogle.com/)

## React

- Use functional components and hooks whenever possible

### Components

Do your best to follow this style. It is based roughly on the [Airbnb style guide]([https://github.com/airbnb/javascript "https://github.com/airbnb/javascript"). Try to keep one component per file.

```js
// imports
import React from "react";

// constants
const SIZE = 40;

// styles
const style = StyleSheet.create({
  container: {
    flex: 1,
  },
});

// Component prop typing
interface Props {
  show: boolean;
}

// Functional arrow component
const MyComponent: React.FC<Props> = ({ show }) => <View>yuh</View>;

export default MyComponent;
```

## Theme

See `theme.ts`. Use the default exported theme object for colors and spacing (`padding`/`margin`)

## Redux

We are using [Redux Toolkit](<[https://redux-toolkit.js.org/tutorials/basic-tutorial](https://redux-toolkit.js.org/tutorials/basic-tutorial)>). For each _slice_ of the store, break the code into up to 4 files:

- Caches**State**: Put TypeScript types here, including the interface of the _slice_
- Caches**Slice**: _Slice_ is the new terminology for _reducer_. It represents an independent part of the redux store. In this file, export the slice as default. Export the actions here too if there is no need for an **Actions** file.
- Caches**Actions**: Put any actions that operate on the _slice_'s state in this file. You can export the _slice_'s actions here and create new ones that are more complex (ie. _thunks_)
- Caches**Selectors**: Selectors are functions that efficiently extract parts of the _slice_'s state for external use. Put them all in this file.

For components that need to interact with redux, use the `useSelector` and `dispatch` hooks:

```js
const dispatch = useDispatch();
const idToken = useSelector(getAuthToken);
```

[React context](<[https://reactjs.org/docs/context.html](https://reactjs.org/docs/context.html)>) is a popular new strategy for FE state management and might be a better approach than redux in some situations. Look at `UserLocationProvider` for an example.

## React native maps

[React native maps](<[https://github.com/react-native-community/react-native-maps](https://github.com/react-native-community/react-native-maps)>) is our map library. Try to keep all map use cases under the `GeoMap` component.
[Google Maps styling wizard](<[https://mapstyle.withgoogle.com/](https://mapstyle.withgoogle.com/)>) is what I've been using to create the `json` config for the map style. If you want to change the style you can import our existing `json` into the wizard.

## React native elements

[React native elements](<[https://react-native-elements.github.io/react-native-elements/docs/overview](https://react-native-elements.github.io/react-native-elements/docs/overview)>) is a helpful component library. It is probably better to use this than to try to create your own component at this point in time. In the future, our custom components will probably wrap the components from **elements**
I have been using the prefix **Geo**- as a convention for component/file names to distinguish custom components from **elements** components.

## React navigation

[React navigation](<[https://reactnavigation.org/docs/getting-started/](https://reactnavigation.org/docs/getting-started/)>) is our nav library. Put navigators in their own files under the `navigation` folder.

## Icons

[Font Awesome](<[https://fontawesome.com/icons?d=gallery](https://fontawesome.com/icons?d=gallery)>) is an icon provider that is used by the **React Native Elements** `Icon` component. For now we are using the free icons. Search for icons on their site and copy the name of the icon into the `name` prop.

```js
<Icon name={"book"} type={"font-awesome-5"} />
```

In the future we will probably create our own icons using `Svg`s and a custom `Icon` component.

## Linting

Use **eslint** and **prettier**.

There's a `lint` script in our `package.json`. There's probably a lot of linting errors in the repo as of writing this that I will get around to fixing. When the errors are manageable again please lint before your merge.

My VS Code settings are below. They might need some reworking.

```json
{
  "editor.formatOnSave": true,
  "editor.tabSize": 2,
  "editor.detectIndentation": true,
  "editor.insertSpaces": true,
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": false
  },
  "[javascriptreact]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": false
  },
  "[typescript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": false
  },
  "[typescriptreact]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": false
  }
}
```
