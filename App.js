/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import Firebase from 'firebase';
import { firebaseAuthStateChanged } from './src/store/auth/AuthActions';
import RootReducer from './src/store/RootReducer';
import RootNavigator from './src/navigation/RootNavigator';
import theme from './src/theme';
import AppLoader from './src/AppLoader';
import firebaseConfig from './firebaseconfig.json';

const NavTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: theme.colors.white,
  },
};

const store = configureStore({
  reducer: RootReducer,
});

// Prevent reinitialization on hot reload
if (Firebase.apps.length === 0) {
  Firebase.initializeApp(firebaseConfig);
}
Firebase.auth().onAuthStateChanged((user) => store.dispatch(firebaseAuthStateChanged(user)));

const App = () => (
  <Provider store={store}>
    <AppLoader>
      <NavigationContainer theme={NavTheme}>
        <RootNavigator />
      </NavigationContainer>
    </AppLoader>
  </Provider>
);

export default App;
