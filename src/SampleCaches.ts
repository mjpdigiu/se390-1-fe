import { Cache } from './store/caches/CachesState';

export const sampleCaches: Cache[] = [
  {
    id: '0',
    name: 'milan.digiuseppe',
    message: 'My first geocache!!!',
    found: new Date(2020, 0, 2, 0, 0, 0, 0).valueOf(),
    lat: 40.7464687,
    lng: -73.9844323,
    isUserCreated: true,

    type: 'Message',
    foundReverseGeocode: 'Milano, Italy',
  },
  {
    id: '1',
    name: 'Will Brown',
    message: 'Has anyone discovered the tunnels under UW? Where do they go? Do ghosts live in there?',
    found: new Date(2019, 9, 20, 0, 0, 0, 0).valueOf(),
    lat: 48.4211108,
    lng: -89.2628881,
    isUserCreated: false,

    type: 'Outdoors',
    foundReverseGeocode: 'Lakehead University, Thunder Bay',
    imageUri: 'https://picsum.photos/50',
  },
  {
    id: '2',
    name: 'Jayson',
    message: 'Geocaching is fun :) I\'m gonna tell all my friends',
    found: new Date(2020, 1, 14, 0, 0, 0, 0).valueOf(),
    lat: 40.7464687,
    lng: -73.9844323,
    isUserCreated: false,

    type: 'Game',
    game: {
      yourMove: true,
    },
    foundReverseGeocode: 'MC Building, Waterloo',
  },
  {
    id: '3',
    name: 'Krisztian',
    message: 'Someone please give us $1 million for this app. We are starving students',
    found: new Date(2020, 1, 8, 0, 0, 0, 0).valueOf(),
    lat: 40.7464687,
    lng: -73.9844323,
    isUserCreated: false,

    type: 'Photography',
    foundReverseGeocode: 'Sleeping Giant trail, Thunder Bay',
    imageUri: 'https://picsum.photos/50',
  },

  {
    id: '4',
    name: 'mark z',
    message: 'Geocaching is fun :) I\'m gonna tell all my friends',
    found: new Date(2020, 7, 15, 0, 0, 0, 0).valueOf(),
    lat: 48.393696,
    lng: -89.282193,
    isUserCreated: false,

    type: 'Game',
    game: {
      yourMove: false,
    },
    foundReverseGeocode: 'Spruce Court, Thunder Bay',
  },
  {
    id: '5',
    name: 'steven jobert',
    message: 'Simple can be harder than complex: You have to work hard to get your thinking clean to make it simple. But it’s worth it in the end because once you get there, you can move mountains. ― Steve',
    found: new Date(2020, 0, 2, 0, 0, 0, 0).valueOf(),
    lat: 48.393243,
    lng: -89.281156,
    isUserCreated: false,

    type: 'Message',
    foundReverseGeocode: 'Spruce Court, Thunder Bay',
  },
];
