const COLOR_NAMES = [
  'white',
  'blue',
  'green',
  'avocado',
  'darkgray',
  'lightgray',
  'mediumgray',
  'black',
  'red',
  'pink',
] as const;
export type Color = typeof COLOR_NAMES[number];

type ColorMap = {
  [K in Color]: string;
}
export const COLOR_MAP: ColorMap = {
  white: '#fff',
  blue: '#2089dc',
  green: '#1b822e',
  avocado: '#abce83',
  darkgray: '#999',
  mediumgray: '#aaa',
  lightgray: '#eee',
  black: '#000',
  red: 'red',
  pink: 'pink',
} as const;

export default {
  colors: COLOR_MAP,
  spacing: {
    small: 8,
    medium: 16,
    large: 24,
    xlarge: 32,
    screenPadding: 20,
  },
} as const;
