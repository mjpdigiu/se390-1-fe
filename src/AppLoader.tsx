/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/camelcase */
import React, { createContext } from 'react';
import * as SplashScreen from 'expo-splash-screen';
import {
  useFonts,
  // Nunito_200ExtraLight,
  // Nunito_300Light,
  Nunito_400Regular,
  Nunito_600SemiBold,
  Nunito_700Bold,
  Nunito_800ExtraBold,
  // Nunito_900Black,
  // Nunito_200ExtraLight_Italic,
  // Nunito_300Light_Italic,
  // Nunito_400Regular_Italic,
  // Nunito_600SemiBold_Italic,
  // Nunito_700Bold_Italic,
  // Nunito_800ExtraBold_Italic,
  // Nunito_900Black_Italic,
} from '@expo-google-fonts/nunito';
import { View, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { Asset } from 'expo-asset';
import { getAuthLoading } from './store/auth/AuthSelectors';

const signInVideoAsset = Asset.fromModule(require('../assets/SigninBackground.mp4'));
const geoLogoAsset = Asset.fromModule(require('../assets/LogoShadow.png'));

interface AssetContext {
  signInVideo: Asset;
  geoLogo: Asset;
}
export const AssetContext = createContext<AssetContext>({
  signInVideo: signInVideoAsset,
  geoLogo: signInVideoAsset,
});

/**
 * Renders loading screen until everything necessary for app is loaded:
 * - fonts
 * - auth token
 * - static assets
 */
const AppLoader: React.FC = ({ children }) => {
  const [fontsLoaded] = useFonts({
    // Nunito_200ExtraLight,
    // Nunito_300Light,
    Nunito_400Regular,
    Nunito_600SemiBold,
    Nunito_700Bold,
    Nunito_800ExtraBold,
    // Nunito_900Black,
    // Nunito_200ExtraLight_Italic,
    // Nunito_300Light_Italic,
    // Nunito_400Regular_Italic,
    // Nunito_600SemiBold_Italic,
    // Nunito_700Bold_Italic,
    // Nunito_800ExtraBold_Italic,
    // Nunito_900Black_Italic,
  });

  const authLoading = useSelector(getAuthLoading);

  // Load static assets
  signInVideoAsset.downloadAsync();
  geoLogoAsset.downloadAsync();

  if (!fontsLoaded || !signInVideoAsset.downloaded || !geoLogoAsset.downloaded || authLoading) {
    return null;
  }

  SplashScreen.hideAsync();

  return (
    <AssetContext.Provider value={{ signInVideo: signInVideoAsset, geoLogo: geoLogoAsset }}>
      <View style={StyleSheet.absoluteFill}>
        {children}
      </View>
    </AssetContext.Provider>
  );
};

export default AppLoader;
