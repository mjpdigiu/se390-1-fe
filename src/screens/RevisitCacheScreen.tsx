import React, { useMemo, useEffect, useRef } from 'react';
import { View } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import MapView, { LatLng } from 'react-native-maps';
import { makeGetSavedCache } from '../store/caches/CachesSelectors';
import GeoMarker from '../components/GeoMarker';
import GeoMap, { UserLocationProvider } from '../components/GeoMap';
import UserLocationMarker from '../components/UserLocationMarker';
import { NavParams } from './CacheDetailScreen';

const RevisitCacheScreen = () => {
  const { params } = useRoute();
  const getSavedCache = makeGetSavedCache((params as NavParams).id);
  const cache = useSelector(getSavedCache);
  const cacheLocation: LatLng = useMemo(() => ({ latitude: cache.lat, longitude: cache.lng }), [cache]);
  const mapRef = useRef<MapView>(null);

  // Animation snaps screen to fit markers
  useEffect(() => {
    setTimeout(() => {
      mapRef!.current?.fitToSuppliedMarkers(
        ['Cache', 'UserLocation'], // `identifier`s
        {
          animated: true,
          edgePadding: {
            top: 100,
            right: 100,
            bottom: 100,
            left: 100,
          },
        }
      );
    }, 800);
  }, []);


  return (
    <View style={{ flex: 1 }}>
      <UserLocationProvider>
        <GeoMap location={cacheLocation} ref={mapRef} rotationEnabled={false}>
          <GeoMarker
            identifier={'Cache'}
            location={cacheLocation}
            cacheType={cache.type}
          />
          <UserLocationMarker />
        </GeoMap>
      </UserLocationProvider>
    </View>
  );
};

export default RevisitCacheScreen;
