import React, {
  useState, useCallback, useEffect, useMemo, useLayoutEffect,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  StyleSheet, View, Image, LayoutChangeEvent,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import { ScrollView } from 'react-native-gesture-handler';
import { ImageInfo } from 'expo-image-picker/build/ImagePicker.types';
import { Icon } from 'react-native-elements';
import theme from '../../theme';
import { setPendingCacheImage } from '../../store/pendingNewCache/PendingCacheActions';
import GeoButton from '../../components/GeoButton';
import ScreenContainer from './ScreenContainer';
import { getPendingCacheImage } from '../../store/pendingNewCache/PendingCacheSelectors';
import ProfileCard from '../../components/ProfileCard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.spacing.screenPadding,
  },
  photoUploadZone: {
    borderWidth: 2,
    borderColor: theme.colors.black,
    borderRadius: 16,
    flex: 1,
    marginTop: theme.spacing.medium,
    height: 300,
    width: 300,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textArea: {
    marginTop: theme.spacing.small,
    fontSize: 20,
    backgroundColor: theme.colors.lightgray,
    borderRadius: theme.spacing.small,
    padding: theme.spacing.medium,
    paddingTop: theme.spacing.medium,
  },
  textAreaFooter: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: theme.spacing.small,
  },
});

const PhotographyScreen: React.FC = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const image = useSelector(getPendingCacheImage);
  const setImage = useCallback((img: ImageInfo) => {
    dispatch(setPendingCacheImage(img));
  }, [dispatch]);

  // Request camera roll permissions
  useEffect(() => {
    (async () => {
      if (Constants.platform!.ios) {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          console.warn('Camera roll permissions not granted');
        }
      }
    })();
  }, []);

  // Determine image height a la "auto"
  const [scrollWidth, setScrollWidth] = useState(0);
  const onScrollViewLayout = useCallback((event: LayoutChangeEvent) => {
    setScrollWidth(event.nativeEvent.layout.width);
  }, [setScrollWidth]);
  const imageHeight = useMemo(() => (image ? (scrollWidth * image.height) / image.width : 0), [image, scrollWidth]);

  const onNext = useCallback(() => {
    navigation.navigate('CacheSubmit');
  }, [dispatch, navigation]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GeoButton
          onPress={onNext}
          title={'Next'}
          titleStyle={{ color: theme.colors.white }}
          disabled={!image}
          type={'clear'}
        />
      ),
    });
  }, [navigation, onNext, image]);

  return (
    <ScreenContainer>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}
        onLayout={onScrollViewLayout}
        contentContainerStyle={{ marginHorizontal: theme.spacing.screenPadding }}
      >
        <ProfileCard me style={{ marginVertical: theme.spacing.medium }} />

        {!image && (
          <View style={styles.photoUploadZone}>
            <Icon
              name={'camera'}
              type={'font-awesome-5'}
              color={theme.colors.black}
              size={30}
              onPress={() => navigation.navigate('CameraModal')}
            />
          </View>
        )}
        {image && (
          <Image
            source={image}
            style={{
              width: '100%',
              height: imageHeight,
            }}
            resizeMode={'contain'}
          />
        )}
      </ScrollView>
    </ScreenContainer>
  );
};

export default PhotographyScreen;
