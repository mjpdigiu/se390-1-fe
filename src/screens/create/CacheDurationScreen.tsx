import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Picker,
  StyleSheet,
  View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import theme from '../../theme';
import { DURATION_OPTIONS } from '../../store/pendingNewCache/PendingCacheSlice';
import { setPendingCacheDuration } from '../../store/pendingNewCache/PendingCacheActions';
import { getPendingCacheDuration } from '../../store/pendingNewCache/PendingCacheSelectors';
import GeoButton from '../../components/GeoButton';
import GeoText from '../../components/GeoText';

const { spacing } = theme;

const styles = StyleSheet.create({
  container: {
    paddingTop: spacing.medium,
    paddingHorizontal: spacing.screenPadding,
  },
});

const CacheDurationScreen = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const cacheDuration = useSelector(getPendingCacheDuration);
  const [duration, setDuration] = useState(cacheDuration);
  const onNext = useCallback(() => {
    dispatch(setPendingCacheDuration(duration));
    navigate('CacheSubmit');
  }, [dispatch, navigate, duration]);

  return (
    <View style={styles.container}>
      <GeoText variant={'blurb'}>
        How long will your geocache be discoverable?
      </GeoText>
      <Picker
        selectedValue={duration}
        onValueChange={(value) => setDuration(value)}
      >
        {DURATION_OPTIONS.map((hours) => (
          <Picker.Item
            label={`${hours} ${hours === 1 ? 'hour' : 'hours'}`}
            value={hours}
            key={hours}
          />
        ))}
      </Picker>
      <GeoButton
        title={'Next'}
        onPress={onNext}
      />
    </View>
  );
};

export default CacheDurationScreen;
