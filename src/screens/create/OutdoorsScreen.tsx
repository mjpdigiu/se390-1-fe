import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem, Icon, Tile } from 'react-native-elements';
import theme from '../../theme';
import ScreenContainer from './ScreenContainer';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const items = [
  {
    title: 'Baseball',
    iconName: 'baseball-ball',
  },
  {
    title: 'Basketball',
    iconName: 'basketball-ball',
  },
  {
    title: 'Biking',
    iconName: 'bicycle',
  },
  {
    title: 'Birdwatching',
    iconName: 'binoculars',
  },
  {
    title: 'Camping',
    iconName: 'campground',
  },
  {
    title: 'Soccer',
    iconName: 'futbol',
  },
  {
    title: 'Hiking',
    iconName: 'hiking',
  },
  {
    title: 'Running',
    iconName: 'running',
  },
  {
    title: 'Swimming',
    iconName: 'swimmer',
  },
  {
    title: 'Volleyball',
    iconName: 'volleyball-ball',
  },
];

const OutdoorsScreen: React.FC = () => {
  const { navigate } = useNavigation();

  return (
    <ScreenContainer>
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
        >
          {items.map(({ title, iconName }) => (
            <Tile
              key={title}
              title={title}
              featured
              imageSrc={{ uri: 'https://picsum.photos/200' }}
              icon={{
                name: iconName,
                type: 'font-awesome-5',
                color: theme.colors.white,
                size: 50,
              }}
              height={200}
              width={200}
              onPress={() => console.warn('yuh')}
            />
          ))}
        </ScrollView>
      </View>
    </ScreenContainer>
  );
};

export default OutdoorsScreen;
