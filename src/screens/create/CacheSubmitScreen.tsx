import React, { useCallback, useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import theme from '../../theme';
import { createCache, geoSearch } from '../../store/caches/CachesActions';
import { getIdToken } from '../../store/auth/AuthSelectors';
import { getUserName } from '../../store/user/UserSelectors';
import { getPendingCacheContent, getPendingCacheDuration } from '../../store/pendingNewCache/PendingCacheSelectors';
import GeoButton from '../../components/GeoButton';
import { getCurrentPosition } from '../../store/location/LocationActions';
import { GeoMapFollow, UserLocationProvider } from '../../components/GeoMap';
import UserLocationMarker from '../../components/UserLocationMarker';
import { clearPendingCache } from '../../store/pendingNewCache/PendingCacheActions';
import ProfileCard from '../../components/ProfileCard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: theme.spacing.medium,
    paddingHorizontal: theme.spacing.screenPadding,
  },
  mapContainer: {
    flex: 1,
    overflow: 'hidden',
  },
});

const CacheSubmitScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const cacheContent = useSelector(getPendingCacheContent);
  const cacheDuration = useSelector(getPendingCacheDuration);

  const userName = useSelector(getUserName);
  const idToken = useSelector(getIdToken);

  const onShare = useCallback(async () => {
    navigation.navigate('Discover');
    dispatch(clearPendingCache());
    const userLocation = await getCurrentPosition();
    dispatch(createCache({
      lat: userLocation.latitude,
      lng: userLocation.longitude,
      message: cacheContent,
      expire: cacheDuration,
      uuid: '1',
      name: userName,
      isUserCreated: true,
    },
    idToken));
    dispatch(geoSearch(userLocation, '1', idToken));
  }, [navigation]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GeoButton
          onPress={onShare}
          title={'Share'}
          titleStyle={{ color: theme.colors.white }}
          type={'clear'}
        />
      ),
    });
  }, [navigation, onShare]);

  return (
    <View style={styles.container}>
      <ProfileCard me style={{ marginBottom: theme.spacing.medium }} />
      <View style={styles.mapContainer}>
        <UserLocationProvider>
          <GeoMapFollow>
            <UserLocationMarker />
          </GeoMapFollow>
        </UserLocationProvider>
      </View>
    </View>
  );
};

export default CacheSubmitScreen;
