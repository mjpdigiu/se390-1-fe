import React, {
  useState, useEffect, useRef, useCallback,
} from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';
import { Camera } from 'expo-camera';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import IconButton from '../../components/IconButton';
import CameraButton from '../../components/CameraButton';
import theme from '../../theme';
import GeoButton from '../../components/GeoButton';
import { ImageInfo } from '../../store/pendingNewCache/PendingCacheSlice';
import { setPendingCacheImage } from '../../store/pendingNewCache/PendingCacheActions';
import GeoText from '../../components/GeoText';

type CameraDirection = 'front' | 'back';

const styles = StyleSheet.create({
  buttonBar: {
    position: 'absolute',
    bottom: 0,
    flex: 1,
    flexDirection: 'row',
    marginVertical: theme.spacing.medium,
    marginHorizontal: theme.spacing.medium,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePreview: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'flex-end',
  },
});

const CameraModal = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [cameraDirection, setCameraDirection] = useState<CameraDirection>('back');
  const [image, setImage] = useState<ImageInfo | null>(null);
  const cameraRef = useRef<Camera>(null);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const flipCamera = useCallback(() => {
    setCameraDirection(cameraDirection === 'back' ? 'front' : 'back');
  }, [cameraDirection, setCameraDirection]);

  const takePicture = useCallback(async () => {
    if (cameraRef) {
      const photo = await cameraRef.current!.takePictureAsync();
      setImage(photo);
    }
  }, [cameraRef, setImage]);

  const onRetake = useCallback(() => {
    setImage(null);
  }, [setImage]);

  const onDone = useCallback(() => {
    dispatch(setPendingCacheImage(image));
    navigation.goBack();
  }, [image, dispatch, navigation]);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    // TODO: close modal?
    return <GeoText>No access to camera</GeoText>;
  }

  if (image) {
    return (
      <ImageBackground source={image} style={styles.imagePreview}>
        <View style={styles.buttonBar}>
          <GeoButton
            title={'Retake'}
            onPress={onRetake}
            style={{ flex: 1, marginRight: theme.spacing.medium }}
          />
          <GeoButton
            title={'Done'}
            onPress={onDone}
            style={{ flex: 1 }}
          />
        </View>
      </ImageBackground>
    );
  }

  return (
    <Camera
      style={{ flex: 1 }}
      type={cameraDirection}
      ref={cameraRef}
    >
      <SafeAreaView style={{ flex: 1, justifyContent: 'flex-end' }}>
        <View style={styles.buttonBar}>
          <View style={styles.buttonContainer} />
          <CameraButton onPress={takePicture} style={styles.buttonContainer} />
          <View style={styles.buttonContainer}>
            <IconButton color={'white'} icon={'sync-alt'} onPress={flipCamera} />
          </View>
        </View>
      </SafeAreaView>
    </Camera>
  );
};

export default CameraModal;
