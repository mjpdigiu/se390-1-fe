import React, {
  useState, useCallback, useEffect, useMemo, useLayoutEffect,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  StyleSheet, TextInput, View, Image, LayoutChangeEvent,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import { ScrollView } from 'react-native-gesture-handler';
import { ImageInfo } from 'expo-image-picker/build/ImagePicker.types';
import theme from '../../theme';
import GeoText, { textStyles } from '../../components/GeoText';
import { setPendingCacheContent, setPendingCacheImage, setPendingCacheTag } from '../../store/pendingNewCache/PendingCacheActions';
import GeoButton from '../../components/GeoButton';
import ScreenContainer from './ScreenContainer';
import IconButton from '../../components/IconButton';
import { getPendingCacheImage, getPendingCacheTag } from '../../store/pendingNewCache/PendingCacheSelectors';
import Tag, { AddTagButton } from '../../components/Tag';
import ProfileCard from '../../components/ProfileCard';

const CHAR_MAX = 100;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.spacing.screenPadding,
  },
  profileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: theme.spacing.medium,
  },
  textArea: {
    marginTop: theme.spacing.small,
    backgroundColor: theme.colors.lightgray,
    borderRadius: theme.spacing.small,
    padding: theme.spacing.medium,
    paddingTop: theme.spacing.medium,
  },
  textAreaFooter: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: theme.spacing.small,
  },
});

const CacheMessageScreen: React.FC = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const image = useSelector(getPendingCacheImage);
  const setImage = useCallback((img: ImageInfo) => {
    dispatch(setPendingCacheImage(img));
  }, [dispatch]);

  // Request camera roll permissions
  useEffect(() => {
    (async () => {
      if (Constants.platform!.ios) {
        const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          console.warn('Camera roll permissions not granted');
        }
      }
    })();
  }, []);

  // Determine image height a la "auto"
  const [scrollWidth, setScrollWidth] = useState(0);
  const onScrollViewLayout = useCallback((event: LayoutChangeEvent) => {
    setScrollWidth(event.nativeEvent.layout.width);
  }, [setScrollWidth]);
  const imageHeight = useMemo(() => (image ? (scrollWidth * image.height) / image.width : 0), [image, scrollWidth]);

  const onPickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      quality: 1,
    });
    if (!result.cancelled) {
      setImage(result);
    }
  };

  const onNext = useCallback(() => {
    dispatch(setPendingCacheContent(text));
    navigation.navigate('CacheSubmit');
  }, [dispatch, navigation, text]);

  const tag = useSelector(getPendingCacheTag);
  const onAddTag = useCallback(() => navigation.navigate('TagScreen'), [navigation]);
  const onRemoveTag = useCallback(() => dispatch(setPendingCacheTag(undefined)), [dispatch]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GeoButton
          onPress={onNext}
          title={'Next'}
          titleStyle={{ color: theme.colors.white }}
          disabled={text.length === 0}
          type={'clear'}
        />
      ),
    });
  }, [navigation, onNext, image]);


  return (
    <ScreenContainer>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}
        onLayout={onScrollViewLayout}
        contentContainerStyle={styles.container}
      >
        <ProfileCard me style={{ marginTop: theme.spacing.medium }} />
        <View style={{ marginTop: theme.spacing.medium }}>
          {tag ? (
            <Tag tag={tag} onRemove={onRemoveTag} />
          ) : (
            <AddTagButton onAdd={onAddTag} />
          )}
        </View>
        <TextInput
          value={text}
          onChangeText={setText}
          multiline
          maxLength={CHAR_MAX}
          style={[styles.textArea, textStyles.blurb]}
          placeholder={'My cache...'}
        />
        <View style={styles.textAreaFooter}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <IconButton icon={'image'} onPress={onPickImage} />
            <IconButton
              icon={'camera'}
              onPress={() => navigation.navigate('CameraModal')}
              style={{ marginLeft: theme.spacing.medium }}
            />
          </View>
          <GeoText variant={'secondaryText'} color={'mediumgray'}>
            {`${CHAR_MAX - text.length}`}
          </GeoText>
        </View>
        {image && (
          <Image
            source={image}
            style={{
              width: '100%',
              height: imageHeight,
            }}
            resizeMode={'contain'}
          />
        )}
      </ScrollView>
    </ScreenContainer>
  );
};

export default CacheMessageScreen;
