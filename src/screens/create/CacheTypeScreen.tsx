import React from 'react';
import TouchableScale from 'react-native-touchable-scale';
import { StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { ListItem, Icon } from 'react-native-elements';
import theme from '../../theme';
import ScreenContainer from './ScreenContainer';
import GeoText from '../../components/GeoText';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

interface CacheTypeListItem {
  title: string;
  subtitle: string;
  colorLeft: string;
  colorRight: string;
  iconName: string;
  nextScreen: string;
}


const CacheTypeScreen: React.FC = () => {
  const { navigate } = useNavigation();

  // TODO: refactor this to get icons/colors from CacheUtils fns
  const items: CacheTypeListItem[] = [
    {
      title: 'Games',
      subtitle: 'Start a game for others to join!',
      colorLeft: '#F44336',
      colorRight: '#FF9800',
      iconName: 'dice-five',
      nextScreen: 'GamesScreen',
    },
    {
      title: 'Activities',
      subtitle: 'Meet up with people in your neighbourhood for a fun outdoor activity!',
      colorLeft: '#004610',
      colorRight: '#00bf8f',
      iconName: 'volleyball-ball',
      nextScreen: 'OutdoorsScreen',
    },
    {
      title: 'Photography',
      subtitle: 'Take a photo of something beautiful!',
      colorLeft: '#267871',
      colorRight: '#136a8a',
      iconName: 'camera',
      nextScreen: 'PhotographyScreen',
    },
    {
      title: 'Message',
      subtitle: 'Leave a message for others to discover!',
      colorLeft: '#D1913C',
      colorRight: '#FFD194',
      iconName: 'font',
      nextScreen: 'CacheContent',
    },
  ];


  return (
    <ScreenContainer>
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
        >
          {items.map((item) => (
            <ListItem
              key={item.title}
              Component={TouchableScale}
              friction={90} //
              tension={100} // These props are passed to the parent component (here TouchableScale)
              activeScale={0.95} //
              linearGradientProps={{
                colors: [item.colorRight, item.colorLeft],
                start: [1, 0],
                end: [0.2, 0],
              }}
              leftAvatar={(
                <Icon
                  name={item.iconName}
                  type={'font-awesome-5'}
                  color={item.colorLeft}
                  raised
                />
              )}
              title={<GeoText color={'white'} variant={'textBold'}>{item.title}</GeoText>}
              subtitle={<GeoText color={'white'} variant={'secondaryText'}>{item.subtitle}</GeoText>}
              chevron={{ color: theme.colors.white }}
              onPress={() => navigate(item.nextScreen)}
              style={{ marginTop: theme.spacing.small }}
            />
          ))}
        </ScrollView>
      </View>
    </ScreenContainer>
  );
};

export default CacheTypeScreen;
