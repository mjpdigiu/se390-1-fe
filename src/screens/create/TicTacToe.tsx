import React from 'react';
import {
  View, StyleSheet, TouchableWithoutFeedback, ViewStyle,
} from 'react-native';
import { Icon } from 'react-native-elements';
import theme from '../../theme';

const BORDER_WIDTH = 1;
const CELL_SIZE = 80;

type CellState = 'X' | 'O' | null;
export type TicTacToeBoardState = CellState[][];
export const INITIAL_TICTACTOE_STATE = [
  [null, null, null],
  [null, null, null],
  [null, null, null],
];
export const SAMPLE_TICTACTOE_STATE = [
  ['X', null, null],
  ['X', 'O', null],
  [null, null, 'O'],
];

const styles = StyleSheet.create({
  cell: {
    width: CELL_SIZE,
    height: CELL_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: BORDER_WIDTH,
    borderStyle: 'solid',
    borderColor: theme.colors.black,
  },
});

interface RowProps {
  row: number;
  rowState: CellState[];
  onCellPress: (row: number, col: number) => void;
}

const Row: React.FC<RowProps> = ({ row, rowState, onCellPress }) => (
  <View style={{ flexDirection: 'row' }}>
    {rowState.map((cellState, col) => (
      <TouchableWithoutFeedback
        key={`${col}`}
        onPress={() => {
          if (cellState === null) {
            onCellPress(row, col);
          }
        }}
      >
        <View
          style={[styles.cell, {
            borderTopWidth: row === 0 ? 0 : BORDER_WIDTH,
            borderBottomWidth: row === 2 ? 0 : BORDER_WIDTH,
            borderLeftWidth: col === 0 ? 0 : BORDER_WIDTH,
            borderRightWidth: col === 2 ? 0 : BORDER_WIDTH,
          }]}
        >
          {cellState && (
            <Icon
              name={cellState === 'O' ? 'globe-americas' : 'arrows-alt'}
              type={'font-awesome-5'}
              color={theme.colors.black}
              size={50}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
    ))}
  </View>
);

interface TicTacToeProps {
  boardState: TicTacToeBoardState;
  onCellPress: (row: number, col: number) => void;
  style?: ViewStyle;
}

const TicTacToe: React.FC<TicTacToeProps> = ({ boardState, onCellPress, style }) => (
  <View style={style}>
    {boardState.map((rowState, rowIndex) => (
      <Row
        key={rowIndex}
        rowState={rowState}
        row={rowIndex}
        onCellPress={onCellPress}
      />
    ))}
  </View>
);

export default TicTacToe;
