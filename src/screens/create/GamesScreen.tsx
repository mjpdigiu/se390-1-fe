import React, { useState, useCallback, useLayoutEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import theme from '../../theme';
import ScreenContainer from './ScreenContainer';
import TicTacToe, { TicTacToeBoardState, INITIAL_TICTACTOE_STATE } from './TicTacToe';
import GeoText from '../../components/GeoText';
import GeoButton from '../../components/GeoButton';
import ProfileCard from '../../components/ProfileCard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  mapContainer: {
    flex: 1,
    width: '100%',
    marginTop: theme.spacing.medium,
  },
});

const GamesScreen: React.FC = () => {
  const navigation = useNavigation();

  const [moveMade, setMoveMade] = useState<boolean>(false);
  const [ticTacToeState, setTicTacToeState] = useState<TicTacToeBoardState>(
    [
      [null, null, null],
      [null, null, null],
      [null, null, null],
    ],
  );

  const onCellPress = useCallback((row: number, col: number) => {
    setMoveMade(true);
    const temp = [...INITIAL_TICTACTOE_STATE] as TicTacToeBoardState;
    const tempRow = [...temp[row]];
    tempRow[col] = 'X';
    temp[row] = tempRow;

    setTicTacToeState(temp);
  }, [setTicTacToeState, setMoveMade]);

  const onNext = useCallback(() => navigation.navigate('CacheSubmit'), [navigation]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GeoButton
          onPress={onNext}
          title={'Next'}
          titleStyle={{ color: theme.colors.white }}
          disabled={!moveMade}
          type={'clear'}
        />
      ),
    });
  }, [navigation, onNext, moveMade]);

  return (
    <ScreenContainer>
      <ProfileCard
        me
        style={{
          marginVertical: theme.spacing.medium,
          paddingHorizontal: theme.spacing.screenPadding,
        }}
      />
      <View style={styles.container}>
        <GeoText color={'blue'} variant={'blurb'}>
          Make the first move
        </GeoText>
        <TicTacToe
          boardState={ticTacToeState}
          onCellPress={onCellPress}
          style={{ marginTop: theme.spacing.large }}
        />
      </View>
    </ScreenContainer>
  );
};

export default GamesScreen;
