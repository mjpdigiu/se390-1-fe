import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { KeyboardAvoidingView } from 'react-native';

const ScreenContainer: React.FC = ({ children }) => (
  <SafeAreaView
    style={{
      flex: 1,
      paddingTop: 0, // remove SAV padding for top nav
    }}
  >
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={'padding'}
      keyboardVerticalOffset={94} // hard coded bc KAV sux
    >
      {children}
    </KeyboardAvoidingView>
  </SafeAreaView>
);

export default ScreenContainer;
