import React from 'react';
import { useSelector } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import GeoText from '../components/GeoText';
import theme from '../theme';

import {
  getUserName,
  getUserAvatarUri,
} from '../store/user/UserSelectors';
import GeoButton from '../components/GeoButton';

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: theme.colors.mediumgray,
    marginTop: theme.spacing.medium,
  },
});

const ProfileScreen = () => {
  const { navigate } = useNavigation();
  const userName = useSelector(getUserName);
  const userAvatarUri = useSelector(getUserAvatarUri);
  const onEditPress = () => navigate('EditProfile');

  return (
    <View style={{ flex: 1, justifyContent: 'space-between' }}>
      <View style={{ alignItems: 'center' }}>
        <Avatar
          rounded
          source={{ uri: userAvatarUri }}
          containerStyle={styles.avatar}
          size={'xlarge'}
        />
        <GeoText
          variant={'blurb'}
          style={{ marginTop: theme.spacing.small }}
        >
          {userName!}
        </GeoText>
        <GeoButton
          fullWidth={false}
          type={'outline'}
          title={'Edit profile'}
          onPress={onEditPress}
          style={{ marginTop: theme.spacing.medium }}
        />
      </View>
    </View>
  );
};

export default ProfileScreen;
