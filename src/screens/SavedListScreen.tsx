import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet, View, SectionListData } from 'react-native';
import theme from '../theme';
import CacheListItem from '../components/CacheListItem';
import { Cache } from '../store/caches/CachesState';
import GeoText from '../components/GeoText';
import {
  getUserCreatedCaches,
  getFoundCaches,
} from '../store/caches/CachesSelectors';
import { getIdToken } from '../store/auth/AuthSelectors';
import { removeSaved, deleteCache } from '../store/caches/CachesActions';
import SwipeableList from '../components/SwipeableList';

const { colors, spacing } = theme;

const styles = StyleSheet.create({
  header: {
    marginTop: spacing.medium,
    marginBottom: spacing.medium,
    marginHorizontal: theme.spacing.screenPadding,
    backgroundColor: colors.white,

    borderColor: colors.green,
    borderStyle: 'solid',
    borderBottomWidth: 1,
  },
});

interface SectionHeaderProps {
  title: string;
}

const SectionHeader: React.FC<SectionHeaderProps> = ({ title }) => (
  <View style={styles.header}>
    <GeoText variant={'formHeader'} color={'green'}>
      {title}
    </GeoText>
  </View>
);

const cacheSort = (a: Cache, b: Cache) => b.found - a.found;

const SavedListScreen: React.FC = () => {
  const dispatch = useDispatch();
  const foundCaches: Cache[] = useSelector(getFoundCaches);
  const userCreatedCaches: Cache[] = useSelector(getUserCreatedCaches);

  const idToken = useSelector(getIdToken);

  const listSections = useMemo(() => ([
    {
      title: 'Found in the Wild',
      data: foundCaches.sort(cacheSort),
    },
    {
      title: 'Created by Me',
      data: userCreatedCaches.sort(cacheSort),
    },
  ]), [foundCaches, userCreatedCaches]);

  const handleDelete = useCallback(
    (cache: Cache) => {
      if (cache.isUserCreated) {
        dispatch(deleteCache(cache.id, idToken));
      } else {
        dispatch(removeSaved(cache.id));
      }
    },
    [],
  );

  const renderSavedMessage = ({
    id,
    found,
    name,
    message,
    isUserCreated,

    type,
    game,
    foundReverseGeocode,
    imageUri,
  }: Cache) => (
    <CacheListItem
      id={id}
      found={found}
      name={isUserCreated ? 'me' : name}
      message={message}

      cacheType={type}
      yourMove={game && game.yourMove}
      foundReverseGeocode={foundReverseGeocode}
      imageUri={imageUri}
    />
  );

  const renderSectionHeader = (section: SectionListData<Cache>) => (
    <SectionHeader title={section.title} />
  );

  return (
    <SwipeableList
      keyExtractor={(cache) => cache.id}
      sections={listSections}
      renderItem={renderSavedMessage}
      renderSectionHeader={renderSectionHeader}
      onDelete={handleDelete}
    />
  );
};

export default SavedListScreen;
