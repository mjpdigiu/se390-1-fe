import React, { useCallback } from 'react';
import { View, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { Button } from 'react-native-elements';
import { useDispatch } from 'react-redux';
import GeoText from '../components/GeoText';
import { EarthSvg, FriendsSvg, PhoneSvg } from '../components/Svg';
import theme from '../theme';
import { setHasBeenOnboarded } from '../store/preferences/PreferencesSlice';

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingVertical: 60, // magic number to make room for pagination dots
    paddingHorizontal: theme.spacing.screenPadding,
  },
  icon: {
    position: 'absolute',
    top: '40%',
  },
});

const OnboardingScreen = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  const onCompletion = useCallback(() => {
    navigate('Home');
    dispatch(setHasBeenOnboarded(true));
  }, [dispatch, navigate]);

  return (
    <Swiper loop={false} activeDotColor={theme.colors.white}>
      <LinearGradient colors={['#1B822E', '#AAEA61']} style={styles.slide}>
        <GeoText variant={'title'} align={'center'} color={'white'}>
          Welcome
        </GeoText>
        <View style={styles.icon}>
          <EarthSvg />
        </View>
        <GeoText variant={'blurb'} align={'center'} color={'white'}>
          With GEO, you can discover virtual geocaches as you explore your neighbourhood and city.
        </GeoText>
      </LinearGradient>

      <LinearGradient colors={['#1B5D82', '#61A8EA']} style={styles.slide}>
        <GeoText variant={'title'} align={'center'} color={'white'}>
          Geocaching
        </GeoText>
        <View style={styles.icon}>
          <PhoneSvg />
        </View>
        <GeoText variant={'blurb'} align={'center'} color={'white'}>
          Traditional geocaching uses GPS coordinates to hide and find physical caches. With GEO, the caches show up on your phone!
        </GeoText>
      </LinearGradient>

      <LinearGradient colors={['#821B7E', '#EA6192']} style={styles.slide}>
        <GeoText variant={'title'} align={'center'} color={'white'}>
          Rules of the Game
        </GeoText>
        <View style={styles.icon}>
          <FriendsSvg />
        </View>
        <View style={{ width: '100%' }}>
          <GeoText variant={'blurb'} align={'center'} color={'white'}>
            Be kind and have fun!
          </GeoText>
          <Button
            title={"Let's get started"}
            onPress={onCompletion}
            style={{ marginTop: theme.spacing.xlarge }}
            buttonStyle={{ backgroundColor: 'white' }}
            titleStyle={{ fontFamily: 'Nunito_700Bold', color: 'black' }}
          />
        </View>
      </LinearGradient>
    </Swiper>
  );
};

export default OnboardingScreen;
