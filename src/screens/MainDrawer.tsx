import React, { useCallback, useMemo } from 'react';
import {
  View, StyleSheet, Group, Alert,
} from 'react-native';
import {
  Avatar, ListItem, Icon,
} from 'react-native-elements';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { getUserAvatarUri, getUserName } from '../store/user/UserSelectors';
import { getGroups } from '../store/groups/GroupSelectors';
import theme from '../theme';
import GeoText from '../components/GeoText';
import GeoButton from '../components/GeoButton';
import { signOutAsync } from '../store/auth/AuthActions';

interface ActionItem {
  icon: string;
  title: string;
  onPress: () => void;
}

const styles = StyleSheet.create({
  drawer: {
    flex: 1,
    backgroundColor: theme.colors.lightgray,
  },
  header: {
    marginVertical: theme.spacing.large,
    paddingHorizontal: theme.spacing.screenPadding,
  },
  logout: {
    position: 'absolute',
    bottom: theme.spacing.small,
    alignSelf: 'center',
  },
});

interface Props {
  onClose: () => void;
}

const MainDrawer: React.FC<Props> = ({ onClose }) => {
  const { navigate } = useNavigation();
  const userAvatarUri = useSelector(getUserAvatarUri);
  const userName = useSelector(getUserName);
  const groups = useSelector(getGroups);

  const onProfilePress = useCallback(() => {
    onClose();
    navigate('Profile');
  }, [onClose, navigate]);

  const onSettingsPress = useCallback(() => {
    onClose();
    navigate('Settings');
  }, [onClose, navigate]);

  const onDevPress = useCallback(() => {
    onClose();
    navigate('Dev');
  }, [onClose, navigate]);

  const onGroupSettings = useCallback(() => {
    // onClose();
    navigate('Group', {});
  }, [onClose, navigate]);

  const onLogoutPress = useCallback(() => Alert.alert(
    userName,
    'Are you sure you want to log out?',
    [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'Log out',
        onPress: signOutAsync,
        style: 'destructive',
      },
    ],
  ), [userName]);

  const items: ActionItem[] = useMemo(() => ([
    {
      icon: 'user-alt',
      title: 'Profile',
      onPress: onProfilePress,
    },
    {
      icon: 'cog',
      title: 'Settings',
      onPress: onSettingsPress,
    },
    {
      icon: 'wrench',
      title: 'Developer',
      onPress: onDevPress,
    },
  ]), [onSettingsPress, onDevPress]);

  return (
    <View style={styles.drawer}>
      <View style={styles.header}>
        <Avatar
          rounded
          size={'large'}
          source={{ uri: userAvatarUri }}
          containerStyle={{ marginBottom: theme.spacing.small }}
        />
        <GeoText variant={'textBold'}>{userName!}</GeoText>
      </View>
      {items.map((item) => (
        <ListItem
          key={item.title}
          leftIcon={(
            <Icon
              name={item.icon}
              type={'font-awesome-5'}
              color={theme.colors.black}
            />
          )}
          title={<GeoText>{item.title}</GeoText>}
          style={{
            marginTop: theme.spacing.small,
          }}
          containerStyle={{
            backgroundColor: theme.colors.white,
          }}
          onPress={item.onPress}
        />
      ))}
      <View>
        <ListItem
          key={'groupheader'}
          title={<GeoText variant={'textBold'}>Groups</GeoText>}
          style={{
            marginTop: theme.spacing.small,
          }}
          containerStyle={{
            backgroundColor: theme.colors.white,
          }}
        />
        {
          groups.map((group) => (
            <ListItem
              key={group.groupId}
              title={<GeoText>{group.groupName}</GeoText>}
              style={{
                marginTop: theme.spacing.small,
                marginLeft: theme.spacing.medium,
              }}
              containerStyle={{
                backgroundColor: theme.colors.white,
              }}
              onPress={() => navigate('Group', { groupId: group.groupId })}
            />
          ))
        }
      </View>
      <View style={styles.logout}>
        <GeoButton title={'Log out'} onPress={onLogoutPress} type={'clear'} />
      </View>
    </View>
  );
};

export default MainDrawer;
