import React from 'react';
import { useSelector } from 'react-redux';
import { useRoute, useNavigation } from '@react-navigation/native';

import {
  View, StyleSheet, FlatList, Text,
} from 'react-native';


import {
  Avatar, ListItem, Icon,
} from 'react-native-elements';
import CacheListItem from '../components/CacheListItem';
import { Cache } from '../store/caches/CachesState';


import GeoText from '../components/GeoText';
import theme from '../theme';

import {
  getUserName,
  getUserAvatarUri,
} from '../store/user/UserSelectors';
import { getGroupMemgers, getGroupCaches, getGroup } from '../store/groups/GroupSelectors';
import { Membership } from '../store/groups/GroupState';

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: theme.colors.mediumgray,
    marginTop: theme.spacing.medium,
  },
});

const renderMember = (item: { item: Membership }) => {
  const member = item.item;
  return (
    <ListItem
      key={member.userId}
      title={member.userId}
      style={{
        marginTop: theme.spacing.small,
        marginLeft: theme.spacing.medium,
        marginRight: theme.spacing.medium,
      }}
      titleStyle={{
        color: member.permissions.admin ? theme.colors.green : theme.colors.black,
      }}
      containerStyle={{
        backgroundColor: member.permissions.admin ? theme.colors.lightgray : theme.colors.white,
      }}
    />
  );
};

const renderCache = (item: {item: Cache}) => {
  const {
    id,
    found,
    name,
    message,
    isUserCreated,

    type,
    game,
    foundReverseGeocode,
    imageUri,
  } = item.item;
  return (
    <CacheListItem
      id={id}
      found={found}
      name={isUserCreated ? 'me' : name}
      message={message}

      cacheType={type}
      yourMove={game && game.yourMove}
      foundReverseGeocode={foundReverseGeocode}
      imageUri={imageUri}
    />
  );
};

const GroupScreen = () => {
  const { params } = useRoute();
  const userName = useSelector(getUserName);
  const userImageUrl = useSelector(getUserAvatarUri);


  const { groupId } = params;
  const group = useSelector(getGroup(groupId));
  const memberList = useSelector(getGroupMemgers(groupId));
  const cacheList = useSelector(getGroupCaches(groupId));

  return (
    <View style={{ flex: 1, justifyContent: 'space-between' }}>
      <View style={{ alignItems: 'center' }}>
        <GeoText
          variant={'title'}
          style={{ marginTop: theme.spacing.small }}
        >
          {group.groupName!}
        </GeoText>
      </View>

      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        <ListItem // todo: make geotext
          key={'cachesheader'}
          title={'Caches'}
          style={{
            marginTop: theme.spacing.small,
            marginLeft: theme.spacing.small,
            marginRight: theme.spacing.small,
          }}
          containerStyle={{
            backgroundColor: theme.colors.white,
          }}
        />
        <FlatList
          data={Object.values(cacheList)}
          renderItem={renderCache}
          keyExtractor={(cache: Cache) => cache.id}
        />

      </View>

      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        <ListItem // todo: make geotext
          key={'membersheader'}
          title={'Members'}
          style={{
            marginTop: theme.spacing.small,
          }}
          containerStyle={{
            backgroundColor: theme.colors.white,
          }}
        />
        <FlatList
          data={Object.values(memberList)}
          renderItem={renderMember}
          keyExtractor={(member: Membership) => member.userId}
        />
      </View>
    </View>
  );
};

export default GroupScreen;
