import React, {
  useState, useMemo, useCallback, useLayoutEffect,
} from 'react';
import { View, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import * as ImagePicker from 'expo-image-picker';
import GeoButton from '../components/GeoButton';
import { getUserName, getUserAvatarUri } from '../store/user/UserSelectors';
import theme from '../theme';
import DismissKeyboardView from '../components/DismissKeyboardView';
import { setUserName, setUserAvatarUri } from '../store/user/UserSlice';
import GeoInput from '../components/GeoInput';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    marginHorizontal: theme.spacing.screenPadding,
  },
  avatar: {
    marginVertical: theme.spacing.medium,
    alignSelf: 'center',
  },
});

const EditProfileModal = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  // Initial profile state
  const initialUserName = useSelector(getUserName);
  const initialAvatarUri = useSelector(getUserAvatarUri);

  // Editing profile state
  const [userNameText, setUserNameText] = useState<string>(initialUserName!);
  const [avatarUri, setAvatarUri] = useState<string>(initialAvatarUri);

  const isUsernameTooLong: boolean = useMemo(() => userNameText.length > 20, [userNameText]);
  const isUsernameValid: boolean = useMemo(() => {
    if (userNameText.length === 0) {
      return false;
    }
    return !isUsernameTooLong;
  }, [userNameText, isUsernameTooLong]);

  const onPickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      quality: 1,
    });
    if (!result.cancelled) {
      setAvatarUri(result.uri);
    }
  };

  const onSavePress = useCallback(() => {
    dispatch(setUserName(userNameText));
    dispatch(setUserAvatarUri(avatarUri));
    navigation.goBack();
  }, [dispatch, navigation, userNameText, avatarUri]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GeoButton
          onPress={onSavePress}
          title={'Save'}
          titleStyle={{ color: theme.colors.white }}
          disabled={!isUsernameValid}
          type={'clear'}
        />
      ),
    });
  }, [navigation, onSavePress, isUsernameValid]);

  return (
    <DismissKeyboardView style={styles.screen}>
      <View>
        <Avatar
          rounded
          source={{ uri: avatarUri }}
          containerStyle={styles.avatar}
          size={'xlarge'}
          showAccessory
          onAccessoryPress={onPickImage}
        />
        <GeoInput
          value={userNameText}
          onChangeText={setUserNameText}
          label={'Username'}
          placeholder={'Add your name'}
          errorMessage={isUsernameTooLong ? 'Must be 20 characters or fewer' : undefined}
        />
      </View>
    </DismissKeyboardView>
  );
};

export default EditProfileModal;
