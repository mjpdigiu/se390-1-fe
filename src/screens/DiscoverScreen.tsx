import React, { useCallback, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { LatLng } from 'react-native-maps';
import { Icon, Avatar } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import Drawer from 'react-native-drawer';
import { Cache } from '../store/caches/CachesState';
import { getDiscoveredCaches } from '../store/caches/CachesSelectors';
import { saveCache, reportCache } from '../store/caches/CachesActions';
import { getIdToken } from '../store/auth/AuthSelectors';
import { getUserAvatarUri } from '../store/user/UserSelectors';
import GeoMarker from '../components/GeoMarker';
import { sampleCaches } from '../SampleCaches';
import { GeoMapSearch, UserLocationProvider } from '../components/GeoMap';
import UserLocationMarker from '../components/UserLocationMarker';
import theme from '../theme';
import { NavParams } from './CacheDetailScreen';
import MainDrawer from './MainDrawer';
import CacheFoundModal from '../components/CacheFoundModal';

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  buttonContainer: {
    width: '100%',
    paddingHorizontal: theme.spacing.screenPadding,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    bottom: theme.spacing.large,
  },
  iconButton: {
    shadowColor: theme.colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

// utils to generate caches for quick testing
const delta = () => (Math.random() - 0.5) * 0.0022;
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const generateCaches = (location: LatLng): Cache[] => sampleCaches.slice(0, 3).map((c) => ({
  ...c,
  lat: location.latitude + delta(),
  lng: location.longitude + delta(),
}));


const DiscoverScreen: React.FC = () => {
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const discoveredCaches: Cache[] = useSelector(getDiscoveredCaches);

  const idToken = useSelector(getIdToken);
  const userAvatarUri = useSelector(getUserAvatarUri);

  const [currCache, setCurrCache] = useState<Cache | null>();
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  const onCreatePress = useCallback(() => navigate('Create'), [navigate]);
  const onSavedPress = useCallback(() => navigate('SavedList'), [navigate]);

  const onSave = useCallback((cache: Cache) => {
    setModalOpen(false);
    dispatch(saveCache({ ...cache, found: new Date().valueOf() }));
  }, [dispatch, setModalOpen]);
  const onReport = useCallback((id) => {
    dispatch(reportCache(id, '1', 'reported', idToken));
  }, [dispatch]);

  const drawerRef = useRef(null);
  const onDrawerOpen = useCallback(() => drawerRef.current!.open(), [drawerRef]);
  const onDrawerClose = useCallback(() => drawerRef.current!.close(), [drawerRef]);

  const onCalloutPress = useCallback((cache: Cache) => {
    setCurrCache(cache);
    setModalOpen(true);
  }, [setModalOpen, setCurrCache]);

  return (
    <Drawer
      ref={drawerRef}
      content={<MainDrawer onClose={onDrawerClose} />}
      tapToClose
      openDrawerOffset={100}
      tweenHandler={Drawer.tweenPresets.parallax}
    >
      <View style={{ flex: 1 }}>
        <UserLocationProvider>
          <GeoMapSearch>
            {discoveredCaches.map((cache) => (
              <GeoMarker
                key={cache.id}
                location={{
                  latitude: cache.lat,
                  longitude: cache.lng,
                }}
                cacheType={cache.type}
                message={cache.message}
                onPress={() => onCalloutPress(cache)}
              />
            ))}
            <UserLocationMarker />
          </GeoMapSearch>
        </UserLocationProvider>
        <View style={styles.buttonContainer}>
          <View style={styles.iconButton}>
            <Avatar
              rounded
              size={64}
              source={{ uri: userAvatarUri }}
              onPress={onDrawerOpen}
            />
          </View>
          <View style={styles.iconButton}>
            <Icon
              name={'feather-alt'}
              type={'font-awesome-5'}
              reverse
              raised
              onPress={onCreatePress}
              color={theme.colors.green}
              size={36}
              containerStyle={{ margin: 0 }}
            />
          </View>
          <View style={styles.iconButton}>
            <Icon
              name={'book'}
              type={'font-awesome-5'}
              raised
              onPress={onSavedPress}
              color={theme.colors.green}
              size={30}
              containerStyle={{ margin: 0 }}
            />
          </View>
        </View>
        <CacheFoundModal
          show={modalOpen}
          cache={currCache!}
          onClose={() => setModalOpen(false)}
          onSave={() => onSave(currCache!)}
        />
      </View>
    </Drawer>
  );
};

export default DiscoverScreen;
