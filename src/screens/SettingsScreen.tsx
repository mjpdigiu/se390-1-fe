import React from 'react';
import { View } from 'react-native';
import GeoText from '../components/GeoText';

const SettingsScreen = () => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <GeoText>settings</GeoText>
  </View>
);

export default SettingsScreen;
