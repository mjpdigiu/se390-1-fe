import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { reverseGeocodeAsync } from 'expo-location';
import { useNavigation } from '@react-navigation/native';
import { getIdToken } from '../store/auth/AuthSelectors';
import { geoSearch } from '../store/caches/CachesActions';
import GeoButton from '../components/GeoButton';
import theme from '../theme';
import { getCurrentPosition } from '../store/location/LocationActions';

const styles = StyleSheet.create({
  fullscreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    marginVertical: theme.spacing.small,
  },
});

const DevScreen: React.FC = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const idToken = useSelector(getIdToken);

  const onGeosearch = useCallback(async () => {
    const userLocation = await getCurrentPosition();
    dispatch(geoSearch(userLocation, '1', idToken));
    navigation.navigate('Discover');
  }, [dispatch, navigation]);

  const onReverseGeocode = useCallback(async () => {
    const userLocation = await getCurrentPosition();
    const results = await reverseGeocodeAsync(userLocation);
    const {
      country,
      region,
      city,
      street,
      name,
    } = results[0];
    alert(`${name} ${street} ${city} ${region} ${country}`); // eslint-disable-line no-alert
  }, []);

  return (
    <View style={styles.fullscreen}>
      <GeoButton
        title={'Geosearch'}
        onPress={onGeosearch}
        style={styles.button}
      />
      <GeoButton
        title={'Reverse Geocode'}
        onPress={onReverseGeocode}
        style={styles.button}
      />
      <GeoButton
        title={'Onboarding'}
        onPress={() => navigation.navigate('Onboarding')}
        style={styles.button}
      />
    </View>
  );
};

export default DevScreen;
