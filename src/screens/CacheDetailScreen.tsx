import React, { useMemo, useCallback } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { LatLng } from 'react-native-maps';
import { ScrollView } from 'react-native-gesture-handler';
import { makeGetSavedCache } from '../store/caches/CachesSelectors';
import theme from '../theme';
import GeoText from '../components/GeoText';
import ProfileCard from '../components/ProfileCard';
import GeoMarker from '../components/GeoMarker';
import GeoMap from '../components/GeoMap';
import GeoButton from '../components/GeoButton';
import TicTacToe, { INITIAL_TICTACTOE_STATE } from './create/TicTacToe';

const { colors, spacing } = theme;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: spacing.screenPadding,
  },
  mapContainer: {
    width: '100%',
    height: 200,
    borderColor: colors.lightgray,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 4,
    overflow: 'hidden',
  },
  revisitButtonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export interface NavParams {
  id: string;
  title: string;
}

const CacheDetailScreen = () => {
  const { params } = useRoute();
  const { id } = params as NavParams;
  const { navigate } = useNavigation();
  const getSavedCache = makeGetSavedCache(id);
  const cache = useSelector(getSavedCache);
  const cacheLocation: LatLng = useMemo(() => ({ latitude: cache.lat, longitude: cache.lng }), [cache]);

  const onRevisitCachePress = useCallback(() => {
    navigate('RevisitCache', { id, title: 'Revisit Cache' } as NavParams);
  }, [navigate, id]);

  return (
    <View style={{ flex: 1, justifyContent: 'space-between' }}>
      <ScrollView contentContainerStyle={styles.container}>
        <ProfileCard initials={cache.name[0]} name={cache.name} style={{ marginTop: theme.spacing.medium }} />
        <View style={{ marginTop: theme.spacing.small }}>
          {cache.type === 'Game' && (
            <View style={{ alignItems: 'center' }}>
              <TicTacToe boardState={INITIAL_TICTACTOE_STATE} onCellPress={() => alert('yuh')} />
            </View>
          )}
          {(cache.type === 'Photography' || cache.type === 'Outdoors') && (
            <View style={{ alignItems: 'center' }}>
              <Image
                style={{ width: 200, height: 200 }}
                source={{ uri: 'https://picsum.photos/200' }}
              />
            </View>
          )}
          {cache.type === 'Message' && <GeoText>{cache.message}</GeoText>}
        </View>
      </ScrollView>
      <View style={styles.mapContainer}>
        <GeoMap location={cacheLocation}>
          <GeoMarker
            location={cacheLocation}
            cacheType={cache.type}
          />
        </GeoMap>
        <View style={styles.revisitButtonContainer}>
          <GeoButton
            title={'Revisit Cache'}
            type={'clear'}
            onPress={onRevisitCachePress}
            style={{
              flex: 0.5,
              marginBottom: theme.spacing.small,
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default CacheDetailScreen;
