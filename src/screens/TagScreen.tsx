import React, { useCallback, useMemo, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';
import theme from '../theme';
import Tag, { TAG_TYPES } from '../components/Tag';
import GeoButton from '../components/GeoButton';
import { setPendingCacheTag } from '../store/pendingNewCache/PendingCacheActions';
import ScreenContainer from './create/ScreenContainer';

const styles = StyleSheet.create({
  tagsContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    marginVertical: theme.spacing.small,
    justifyContent: 'center',
  },
});

const TagScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [tag, setTag] = useState<Tag | undefined>();

  const onSubmit = useCallback(() => {
    dispatch(setPendingCacheTag(tag));
    navigation.goBack();
  }, [navigation, tag]);

  const rows = useMemo(() => {
    const result = [];
    const tags = TAG_TYPES.slice();
    while (tags.length > 0) {
      const count: number = (result.length % 2 === 0) ? 3 : 2;
      result.push(tags.splice(0, count));
    }
    return result;
  }, [TAG_TYPES]);

  return (
    <ScreenContainer>
      <View style={styles.tagsContainer}>
        {rows.map((row) => (
          <View key={row[0]} style={styles.row}>
            {row.map((f) => (
              <TouchableWithoutFeedback
                key={f}
                onPress={() => setTag(f)}
                style={{ marginHorizontal: theme.spacing.small }}
              >
                <Tag
                  tag={f}
                  size={'large'}
                  style={{
                    borderColor: f === tag ? theme.colors.black : 'transparent',
                    borderWidth: 2,
                    borderStyle: 'solid',
                  }}
                />
              </TouchableWithoutFeedback>
            ))}
          </View>
        ))}
      </View>
      <GeoButton
        title={'Choose'}
        onPress={onSubmit}
        disabled={!tag}
        style={{ marginVertical: theme.spacing.medium, marginHorizontal: theme.spacing.screenPadding }}
      />
    </ScreenContainer>
  );
};

export default TagScreen;
