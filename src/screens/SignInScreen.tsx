import React, { useContext } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import { Video } from 'expo-av';
import { signInAsyncFacebook, signInAsyncGoogle } from '../store/auth/AuthActions';
import theme from '../theme';
import { AssetContext } from '../AppLoader';

const styles = StyleSheet.create({
  backgroundVideo: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 1,
  },
  logo: {
    flex: 1,
    width: 300,
    height: 107,
    alignSelf: 'center',
    resizeMode: 'contain',
    zIndex: 2,
  },
  buttonContainer: {
    width: '100%',
    paddingHorizontal: theme.spacing.screenPadding,
    paddingVertical: theme.spacing.large,
    zIndex: 2,
  },
});

const SignInScreen = () => {
  const { signInVideo, geoLogo } = useContext(AssetContext);
  return (
    <Video
      source={signInVideo}
      shouldPlay
      isLooping
      style={styles.backgroundVideo}
      resizeMode={'cover'}
    >
      {/* TODO: Replace GEO image with svg once shadows are supported */}
      <Image
        source={{ uri: geoLogo.localUri! }}
        style={styles.logo}
      />
      <View style={styles.buttonContainer}>
        {/* TODO: create SocialIcon component/wrapper and apply our font styles */}
        <SocialIcon
          title={'Sign in with Facebook'}
          button
          type={'facebook'}
          onPress={signInAsyncFacebook}
        />
        <SocialIcon
          title={'Sign in with Google'}
          button
          type={'google'}
          onPress={signInAsyncGoogle}
        />
      </View>
    </Video>
  );
};
export default SignInScreen;
