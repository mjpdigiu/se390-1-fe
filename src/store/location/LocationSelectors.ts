import { RootState } from '../RootReducer';

export const getUserLatLng = (state: RootState) => state.location.userLatLng;
