import { LatLng } from 'react-native-maps';

export default interface LocationState {
  userLatLng: LatLng;
  // weird ass semicolon lint error below
// eslint-disable-next-line semi
}
