import { createSlice } from '@reduxjs/toolkit';
import LocationState from './LocationState';

// NOTE: currently deprecated
const locationSlice = createSlice({
  name: 'location',
  initialState: {
    userLatLng: {
      latitude: 0,
      longitude: 0,
    },
  } as LocationState,
  reducers: {
    setUserLatLng(state, action) {
      state.userLatLng = action.payload;
    },
  },
});

export default locationSlice;
