import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import locationSlice from './LocationSlice';

export const {
  setUserLatLng,
} = locationSlice.actions;

export const getCurrentPosition = async () => {
  const { status } = await Permissions.askAsync(Permissions.LOCATION);
  if (status !== 'granted') {
    console.warn('Location permissions not granted');
  }
  const location = await Location.getCurrentPositionAsync({});
  return {
    latitude: location.coords.latitude,
    longitude: location.coords.longitude,
  };
};
