import { createSlice } from '@reduxjs/toolkit';
import { sampleCaches } from '../../SampleCaches';
import { CachesState } from './CachesState';
import { sampleGroups } from '../../SampleGroups';
import { GroupState } from './GroupState';

const groupsSlice = createSlice({
  name: 'groups',
  initialState: {
    groups: {
      [sampleGroups[0].groupId]: sampleGroups[0],
      [sampleGroups[1].groupId]: sampleGroups[1],
    },
  } as GroupState,
  reducers: {
    /*
    addSaved(state, action) {
      const cache = action.payload;
      state.saved[cache.id] = cache;
    },
    removeSaved(state, action) {
      delete state.saved[action.payload];
    },
    addDiscovered(state, action) {
      const cache = action.payload;
      state.discovered[cache.id] = cache;
    },
    removeDiscovered(state, action) {
      delete state.discovered[action.payload];
    },
  */
  },
});

export default groupsSlice;
