import { User } from 'firebase';
import { UserInterfaceIdiom } from 'expo-constants';


export interface GroupPermissions {
  admin: boolean;
}

export interface Membership {
  userId: string; // correct type ??
  permissions: GroupPermissions;
}

export interface Group {
  groupId: string;
  groupName: string;
  memberList: Membership[];
  cacheIdList: string[]; // just reference caches.. don't need to duplicate info
}

export interface GroupState {
  groups: {
    [id: string]: Group;
  };
}
