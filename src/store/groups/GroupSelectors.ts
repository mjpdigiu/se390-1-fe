import { createSelector } from '@reduxjs/toolkit';
import { State } from 'react-native-gesture-handler';
import { getDiscoveredCaches } from '../caches/CachesSelectors';
import { RootState } from '../RootReducer';

export const getGroups = (state: RootState) => Object.values(state.groups.groups);

export const getGroup = (id: string) => createSelector(
  [getGroups],
  (groups) => groups[id],
);

export const getGroupMemgers = (id: string) => createSelector(
  [getGroups],
  (groups) => groups[id].memberList,
);

export const getGroupCaches = (id: string) => createSelector(
  [
    getGroups,
    (state) => state.caches.saved,
  ],
  (groups, caches) => {
    const result: Cache[] = [];
    for (const c in Object.values(groups[id].cacheIdList)) {
      result.push(caches[c]);
    }
    return result;
  },
);
