import { RootState } from '../RootReducer';

export const getUserName = (state: RootState) => state.user.name;
export const getUserAvatarUri = (state: RootState) => state.user.avatarUri;
