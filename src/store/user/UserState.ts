export interface UserState {
  name: string;
  avatarUri: string;
}
