import { createSlice } from '@reduxjs/toolkit';
import { UserState } from './UserState';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    name: '',
    avatarUri: '',
  } as UserState,
  reducers: {
    setName(state, action) {
      state.name = action.payload;
    },
    setAvatarUri(state, action) {
      state.avatarUri = action.payload;
    },
  },
});

export default userSlice;

export const {
  setName: setUserName,
  setAvatarUri: setUserAvatarUri,
} = userSlice.actions;
