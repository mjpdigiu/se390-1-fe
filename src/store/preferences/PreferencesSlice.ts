import { createSlice } from '@reduxjs/toolkit';
import { PreferencesState } from './PreferencesState';

// TODO: persist
const preferencesSlice = createSlice({
  name: 'preferences',
  initialState: {
    hasBeenOnboarded: false,
  } as PreferencesState,
  reducers: {
    setHasBeenOnboarded(state, action) {
      state.hasBeenOnboarded = action.payload;
    },
  },
});

export default preferencesSlice;

export const {
  setHasBeenOnboarded,
} = preferencesSlice.actions;
