import { RootState } from '../RootReducer';

export const getHasBeenOnboarded = (state: RootState) => state.preferences.hasBeenOnboarded;
