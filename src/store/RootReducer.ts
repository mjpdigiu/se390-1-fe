import { combineReducers } from '@reduxjs/toolkit';
import authSlice from './auth/AuthSlice';
import cachesSlice from './caches/CachesSlice';
import locationSlice from './location/LocationSlice';
import pendingCacheSlice from './pendingNewCache/PendingCacheSlice';
import groupsSlice from './groups/GroupSlice';
import userSlice from './user/UserSlice';
import preferencesSlice from './preferences/PreferencesSlice';

const rootReducer = combineReducers({
  auth: authSlice.reducer,
  caches: cachesSlice.reducer,
  location: locationSlice.reducer,
  pendingCache: pendingCacheSlice.reducer,
  preferences: preferencesSlice.reducer,
  groups: groupsSlice.reducer,
  user: userSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
