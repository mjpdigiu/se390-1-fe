import { Dispatch } from '@reduxjs/toolkit';
import Firebase from 'firebase';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
import AuthSlice from './AuthSlice';
import { setUserAvatarUri, setUserName } from '../user/UserSlice';

const {
  signIn,
  signOut,
} = AuthSlice.actions;

/**
 * Observes changes to Firebase auth state, namely when the user signs in or out
 */
export const firebaseAuthStateChanged = (user: firebase.User) => async (dispatch: Dispatch) => {
  if (user) {
    const idToken = await user.getIdToken();
    dispatch(setUserName(user.displayName));
    dispatch(setUserAvatarUri(user.photoURL));
    dispatch(signIn(idToken));
  } else {
    dispatch(signOut());
  }
};

/**
 * Sign in with Facebook API
 * Prompts user to sign in to Facebook account then creates credential for Firebase
 */
export const signInAsyncFacebook = async () => {
  await Facebook.initializeAsync('710462976370982', undefined);

  Facebook.logInWithReadPermissionsAsync({
    permissions: ['public_profile', 'email'],
  }).then((result) => {
    if (result.type === 'success') {
      const credential = Firebase.auth.FacebookAuthProvider.credential(result.token);
      Firebase.auth().signInWithCredential(credential).catch((error) => {
        console.warn(`Firebase auth error : ${error}`);
      });
    } else {
      // User cancelled login. Don't need to do anything
    }
  }).catch((error) => {
    console.warn('Facebook login error');
    console.warn(error);
  });
};

/**
 * Sign in with Google API
 * Prompts user to sign in to Google account then creates credential for Firebase
 */
export const signInAsyncGoogle = () => {
  Google.logInAsync({
    androidClientId: '834336886747-tdkqs7n2nrr2kgib3efdu253fuopbbgh.apps.googleusercontent.com',
    iosClientId: '834336886747-qktght10v1b1o4pmd7sv0m7gmkfp8bci.apps.googleusercontent.com',
    scopes: ['profile', 'email'],
  }).then((result) => {
    if (result.type === 'success') {
      const credential = Firebase.auth.GoogleAuthProvider.credential(result.idToken, result.accessToken);
      Firebase.auth().signInWithCredential(credential).catch((error) => {
        console.warn(`Firebase auth error : ${error}`);
      });
    } else {
      // User cancelled login. Don't need to do anything
    }
  }).catch((error) => {
    console.warn('Google login error');
    console.warn(error);
  });
};

export const signOutAsync = () => Firebase.auth().signOut();
