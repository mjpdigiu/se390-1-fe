import { createSlice } from '@reduxjs/toolkit';
import { AuthState } from './AuthState';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    idToken: undefined,
    isLoading: true,
  } as AuthState,
  reducers: {
    signIn(state, action) {
      state.idToken = action.payload;
      state.isLoading = false;
    },
    signOut(state) {
      state.idToken = undefined;
      state.isLoading = false;
    },
  },
});

export default authSlice;
