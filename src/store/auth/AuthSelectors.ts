import { RootState } from '../RootReducer';

export const getIdToken = (state: RootState) => state.auth.idToken;
export const getAuthLoading = (state: RootState) => state.auth.isLoading;
