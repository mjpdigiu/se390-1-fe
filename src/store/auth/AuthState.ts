export interface AuthState {
  isLoading: boolean;
  idToken: string | undefined;
}
