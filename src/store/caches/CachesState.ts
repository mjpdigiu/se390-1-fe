export type CacheType = 'Game' | 'Outdoors' | 'Photography' | 'Message'
export interface CacheGame {
  yourMove: boolean;
}

export interface Cache {
  // uuid?: string;
  id: string;
  expire?: number;
  found: number; // timestamp
  lat: number;
  lng: number;
  name: string;
  message: string;
  isUserCreated?: boolean;

  // Tentative new fields
  type?: CacheType;
  game?: CacheGame;
  foundReverseGeocode?: string;
  imageUri?: string;
}

export interface CachesState {
  discovered: {
    [id: string]: Cache;
  };
  saved: {
    [id: string]: Cache;
  };
}

export interface ReportRequest {
  id: string;
  uuid: number;
  reason: string;
}
