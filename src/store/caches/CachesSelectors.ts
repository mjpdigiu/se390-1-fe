import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../RootReducer';

const getSavedCaches = (state: RootState) => Object.values(state.caches.saved);

export const getUserCreatedCaches = createSelector([getSavedCaches],
  (saved) => saved.filter((c) => c.isUserCreated));

export const getFoundCaches = createSelector([getSavedCaches],
  (saved) => saved.filter((c) => !c.isUserCreated));

export const makeGetSavedCache = (id: string) => createSelector(
  [(state: RootState) => state.caches.saved],
  (saved) => saved[id],
);

export const getDiscoveredCaches = (state: RootState) => Object.values(state.caches.discovered);
