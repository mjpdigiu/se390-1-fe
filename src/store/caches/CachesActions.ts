import { Dispatch } from '@reduxjs/toolkit';
import { LatLng } from 'react-native-maps';
import cachesSlice from './CachesSlice';
import { Cache } from './CachesState';

export const {
  addSaved,
  removeSaved,
  addDiscovered,
  removeDiscovered,
} = cachesSlice.actions;

const backendURL = 'http://45.62.247.67:3000';

export const createCache = (cache: Cache, idToken: string) => async (dispatch: Dispatch) => {
  const result = await fetch(`${backendURL}/message`
      + `?idtoken=${encodeURIComponent(idToken)}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(cache),
  });
  if (!result.ok) {
    console.warn(result);
  }
  const { id } = await result.json();
  dispatch(addSaved({
    ...cache,
    id,
  }));
};

export const deleteCache = (id: string, idToken: string) => async (dispatch: Dispatch) => {
  dispatch(removeSaved(id));
  fetch(`${backendURL}/message`
        + `?idtoken=${encodeURIComponent(idToken)}`,
    {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id, uuid: 1 }),
    });
};

export const geoSearch = (location: LatLng, uuid: string, idToken: string) => async (dispatch: Dispatch) => {
  const response = await fetch(`${backendURL}/message/geosearch`
      + `?lat=${encodeURIComponent(location.latitude)}
      &lng=${encodeURIComponent(location.longitude)}
      &uuid=${encodeURIComponent(uuid)}
      &idtoken=${encodeURIComponent(idToken)}`,
  {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  const caches = await response.json();
  caches.forEach((c: Cache) => dispatch(addDiscovered(c)));
};

export const reportCache = (
  id: string,
  uuid: string,
  reason: string,
  idToken : string
) => async (dispatch: Dispatch) => {
  dispatch(removeDiscovered(id));
  const result = await fetch(`${backendURL}/report`
        + `?idtoken=${encodeURIComponent(idToken)}`,
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id, uuid, reason }),
    });
  if (!result.ok) {
    console.warn(result);
  }
};

export const saveCache = (cache: Cache) => (dispatch: Dispatch) => {
  dispatch(removeDiscovered(cache.id));
  dispatch(addSaved(cache));
};
