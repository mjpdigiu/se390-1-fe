import { createSlice } from '@reduxjs/toolkit';
import { sampleCaches } from '../../SampleCaches';
import { CachesState } from './CachesState';

const cachesSlice = createSlice({
  name: 'caches',
  initialState: {
    discovered: {
      4: sampleCaches[4],
      5: sampleCaches[5],
    },
    saved: {
      0: sampleCaches[0],
      1: sampleCaches[1],
      2: sampleCaches[2],
      3: sampleCaches[3],
    },
  } as CachesState,
  reducers: {
    addSaved(state, action) {
      const cache = action.payload;
      state.saved[cache.id] = cache;
    },
    removeSaved(state, action) {
      delete state.saved[action.payload];
    },
    addDiscovered(state, action) {
      const cache = action.payload;
      state.discovered[cache.id] = cache;
    },
    removeDiscovered(state, action) {
      delete state.discovered[action.payload];
    },
  },
});

export default cachesSlice;
