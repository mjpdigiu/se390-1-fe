import { RootState } from '../RootReducer';

export const getPendingCacheContent = (state: RootState) => state.pendingCache.content;
export const getPendingCacheDuration = (state: RootState) => state.pendingCache.duration;
export const getPendingCacheImage = (state: RootState) => state.pendingCache.image;
export const getPendingCacheTag = (state: RootState) => state.pendingCache.tag;
