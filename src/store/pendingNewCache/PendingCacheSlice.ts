import { createSlice } from '@reduxjs/toolkit';
import Tag from '../../components/Tag';

export const DURATION_OPTIONS = [1, 4, 12, 24] as const;
type DurationOption = typeof DURATION_OPTIONS[number]

export interface ImageInfo {
  width: number;
  height: number;
  uri: string;
}

export interface PendingNewCacheState {
  content: string;
  duration: DurationOption;
  image?: ImageInfo;
  tag?: Tag;
}

const pendingCacheSlice = createSlice({
  name: 'pendingNewCache',
  initialState: {
    content: '',
    duration: 1,
  } as PendingNewCacheState,
  reducers: {
    setContent(state, action) {
      state.content = action.payload;
    },
    setDuration(state, action) {
      state.duration = action.payload;
    },
    setImage(state, action) {
      state.image = action.payload;
    },
    setTag(state, action) {
      state.tag = action.payload;
    },
  },
});

export default pendingCacheSlice;
