import { Dispatch } from '@reduxjs/toolkit';
import pendingCacheSlice from './PendingCacheSlice';

export const {
  setContent: setPendingCacheContent,
  setDuration: setPendingCacheDuration,
  setImage: setPendingCacheImage,
  setTag: setPendingCacheTag,
} = pendingCacheSlice.actions;

export const clearPendingCache = () => (dispatch: Dispatch) => {
  dispatch(setPendingCacheContent(''));
  dispatch(setPendingCacheDuration(1));
  dispatch(setPendingCacheImage(undefined));
  dispatch(setPendingCacheTag(undefined));
};
