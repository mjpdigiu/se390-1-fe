import { CacheType } from '../store/caches/CachesState';

export const cacheTypeToIconName = (cacheType: CacheType) => {
  switch (cacheType) {
    case 'Game':
      return 'dice-five';
    case 'Message':
      return 'font';
    case 'Outdoors':
      return 'hiking';
    case 'Photography':
      return 'camera';
    default:
      return '';
  }
};

export const cacheTypeToColorGradient = (cacheType: CacheType) => {
  switch (cacheType) {
    case 'Game':
      return ['#F44336', '#FF9800'];
    case 'Message':
      return ['#D1913C', '#FFD194'];
    case 'Outdoors':
      return ['#004610', '#00bf8f'];
    case 'Photography':
      return ['#267871', '#136a8a'];
    default:
      return '';
  }
};
