import React from 'react';
import { View } from 'react-native';
import theme from '../theme';

type Size = 'small' | 'medium' | 'large' | 'xlarge';
interface Props {
  size?: Size;
}

const Spacer: React.FC<Props> = ({ size = 'medium' }) => (
  <View style={{ height: theme.spacing[size] }} />
);

export default Spacer;
