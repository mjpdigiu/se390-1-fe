import React from 'react';
import {
  TouchableWithoutFeedback, Keyboard, View, ViewStyle,
} from 'react-native';

interface Props {
  style?: ViewStyle;
}

/**
 * Utility component that dismisses keyboard when tapped
 */
const DismissKeyboardView: React.FC<Props> = ({ children, style }) => (
  <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    <View style={style}>
      {children}
    </View>
  </TouchableWithoutFeedback>
);

export default DismissKeyboardView;
