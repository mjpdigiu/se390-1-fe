import React from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';
import { Avatar } from 'react-native-elements';
import { useSelector } from 'react-redux';
import theme from '../theme';
import GeoText from './GeoText';
import { getUserAvatarUri, getUserName } from '../store/user/UserSelectors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

interface Props {
  me?: boolean;
  initials?: string;
  name?: string;
  size?: 'small' | 'medium';
  style?: ViewStyle;
}

const ProfileCard: React.FC<Props> = ({
  me = true,
  initials,
  name,
  size = 'small',
  style,
}) => {
  const myImageUrl = useSelector(getUserAvatarUri);
  const myName = useSelector(getUserName);

  return (
    <View style={[style, styles.container]}>
      {me ? (
        <Avatar
          rounded
          size={size}
          source={{ uri: myImageUrl }}
        />
      ) : (
        <Avatar
          rounded
          title={initials!.toLocaleUpperCase()}
          containerStyle={{ backgroundColor: theme.colors.mediumgray }}
          size={size}
        />
      )}
      <GeoText
        variant={'textBold'}
        style={{ marginLeft: theme.spacing.medium }}
      >
        {me ? myName : name!}
      </GeoText>
    </View>
  );
};
export default ProfileCard;
