import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import { ViewStyle } from 'react-native';
import { Color, COLOR_MAP } from '../theme';

const DEFAULT_SIZE = 20;

interface Props {
  color?: Color;
  icon: string;
  onPress: () => void;
  size?: number;
  style?: ViewStyle;
}

/**
 * This shit need to be fixed.
 * RN Elements Icon has a fuckin onPress prop
 */
const IconButton: React.FC<Props> = ({
  color = 'black',
  icon,
  onPress,
  size = DEFAULT_SIZE,
  style,
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={[style, { width: DEFAULT_SIZE, height: DEFAULT_SIZE }]}
  >
    <Icon
      color={COLOR_MAP[color]}
      name={icon}
      size={size}
      type={'font-awesome-5'}
    />
  </TouchableOpacity>
);
export default IconButton;
