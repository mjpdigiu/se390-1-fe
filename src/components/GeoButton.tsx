import React from 'react';
import { StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import theme from '../theme';
import { textStyles } from './GeoText';

const styles = StyleSheet.create({
  button: {
    borderRadius: theme.spacing.large,
    paddingHorizontal: theme.spacing.medium,
  },
  fullWidth: {
    alignSelf: 'stretch',
  },
});

interface Props {
  disabled?: boolean;
  fullWidth?: boolean;
  icon?: string;
  onPress: () => void;
  style?: ViewStyle;
  title: string;
  titleStyle?: TextStyle;
  type?: 'solid' | 'clear' | 'outline';
}

const GeoButton: React.FC<Props> = ({
  disabled = false,
  fullWidth = true,
  icon,
  style,
  title, onPress,
  titleStyle,
  type = 'solid',
}) => (
  <Button
    buttonStyle={[styles.button]}
    containerStyle={[fullWidth ? styles.fullWidth : null, style]}
    disabled={disabled}
    icon={icon ? (
      <Icon
        name={icon}
        type={'font-awesome-5'}
        color={theme.colors.white}
        style={{ marginRight: theme.spacing.medium }}
      />
    ) : undefined}
    iconContainerStyle={{ marginRight: theme.spacing.xlarge }}
    onPress={onPress}
    title={title}
    titleStyle={[titleStyle, textStyles.button]}
    type={type}
  />
);

export default GeoButton;
