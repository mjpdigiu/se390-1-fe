import React, { useMemo } from 'react';
import { Callout, Marker, LatLng } from 'react-native-maps';
import { View, StyleSheet, ViewStyle } from 'react-native';
import { Icon } from 'react-native-elements';
import GeoText from './GeoText';
import theme from '../theme';
import { CacheType } from '../store/caches/CachesState';
import { cacheTypeToColorGradient, cacheTypeToIconName } from '../utils/CacheUtils';

const ICON_SIZE = 40;
const BUBBLE_SIZE = 80;
const BORDER_COLOR = theme.colors.black;
const BORDER_WIDTH = 2;

const styles = StyleSheet.create({
  bubble: {
    height: BUBBLE_SIZE,
    width: BUBBLE_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: BUBBLE_SIZE,
    borderColor: BORDER_COLOR,
    borderWidth: BORDER_WIDTH,
  },
  pin: {
    alignSelf: 'center',
    width: BORDER_WIDTH,
    height: 30,
    backgroundColor: BORDER_COLOR,
  },
  callout: {
    width: 200,
    padding: theme.spacing.small,
  },
});

interface BubbleProps {
  cacheType: CacheType;
  style?: ViewStyle;
}

export const GeoMarkerBubble: React.FC<BubbleProps> = ({ cacheType, style }) => {
  const iconName = useMemo(() => cacheTypeToIconName(cacheType!), [cacheType]);
  const iconColor = useMemo(() => cacheTypeToColorGradient(cacheType!)[0], [cacheType]);

  return (
    <View style={[style, styles.bubble, { backgroundColor: iconColor }]}>
      <Icon
        color={theme.colors.white}
        name={iconName}
        type={'font-awesome-5'}
        size={ICON_SIZE}
      />
    </View>
  );
};


interface GeoMarkerProps {
  location: LatLng;
  message?: string;
  onPress?: () => void;
  identifier?: string;
  cacheType?: CacheType;
}

const GeoMarker: React.FC<GeoMarkerProps> = ({
  location,
  message,
  onPress,
  identifier,
  cacheType = 'Game', // Temporary default
}) => (
  <Marker coordinate={location} identifier={identifier} onPress={onPress}>
    <View>
      <GeoMarkerBubble cacheType={cacheType} />
      <View style={styles.pin} />
    </View>
    { false && message && onPress && (
      <Callout
        onPress={onPress}
        tooltip={false}
        style={styles.callout}
      >
        <GeoText

          variant={'text'}
          style={{ flex: 1, flexWrap: 'wrap' }}
          numberOfLines={3}
        >
          {message}
        </GeoText>
      </Callout>
    )}
  </Marker>
);
export default GeoMarker;
