import React from 'react';
import { Input } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import { textStyles } from './GeoText';
import theme from '../theme';

const styles = StyleSheet.create({
  label: {
    ...textStyles.textBold,
    color: theme.colors.black,
  },
});

interface Props {
  value: string;
  onChangeText: (text: string) => void;
  label?: string;
  placeholder?: string;
  errorMessage?: string;
}

const GeoInput: React.FC<Props> = ({
  value,
  onChangeText,
  label,
  placeholder,
  errorMessage,
}) => (
  <Input
    value={value}
    onChangeText={onChangeText}
    label={label}
    labelStyle={styles.label}
    placeholder={placeholder}
    errorMessage={errorMessage}
    inputStyle={textStyles.text}
  />
);

export default GeoInput;
