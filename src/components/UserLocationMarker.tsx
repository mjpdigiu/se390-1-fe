import React, { useContext } from 'react';
import { Marker } from 'react-native-maps';
import { Icon } from 'react-native-elements';
import theme from '../theme';
import { UserLocationContext } from './GeoMap';

const UserLocationMarker: React.FC = () => {
  const { userLocation } = useContext(UserLocationContext);
  return (
    <Marker coordinate={userLocation} identifier={'UserLocation'}>
      <Icon
        color={theme.colors.black}
        name={'male'}
        type={'font-awesome-5'}
        size={48}
      />
    </Marker>
  );
};
export default UserLocationMarker;
