import React, { ReactElement, useCallback } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  ListRenderItemInfo,
  SectionListData,
} from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import theme from '../theme';
import GeoText from './GeoText';

const DELETE_BTN_WIDTH = 75;

const styles = StyleSheet.create({
  hidden: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: theme.colors.red,
  },
  backRightBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: DELETE_BTN_WIDTH,
  },
});

interface RowItemHiddenProps<T> {
  item: T;
  onDelete: (item: T) => void;
}

const RowItemHidden = <T extends unknown>({ item, onDelete }: RowItemHiddenProps<T>) => (
  <View style={styles.hidden}>
    <TouchableOpacity
      style={styles.backRightBtn}
      onPress={() => onDelete(item)}
    >
      <GeoText color={'white'} variant={'textBold'}>Delete</GeoText>
    </TouchableOpacity>
  </View>
);

interface BaseProps<T> {
  keyExtractor: (item: T) => string;
  renderItem: (item: T) => ReactElement;
  onDelete: (item: T) => void;
  ListHeaderComponent?: ReactElement;
}

interface SwipeableListProps<T> extends BaseProps<T> {
  items: T[];
}

interface Section<T> {
  title: string;
  data: T[];
}

interface SwipeableSectionListProps<T> extends BaseProps<T> {
  sections: Section<T>[];
  renderSectionHeader: (section: SectionListData<T>) => ReactElement;
}

type Props<T> = SwipeableListProps<T> | SwipeableSectionListProps<T>
const isSwipeableList = <T extends unknown>(props: Props<T>): props is SwipeableListProps<T> => 'items' in props;

const SwipeableList = <T extends unknown>(props: Props<T>) => {
  const {
    keyExtractor, renderItem, onDelete, ListHeaderComponent,
  } = props;

  const renderHiddenItemCallback = useCallback((row: ListRenderItemInfo<T>) => (
    <RowItemHidden item={row.item} onDelete={onDelete} />
  ), [onDelete]);

  const renderItemCallback = useCallback(
    (row: ListRenderItemInfo<T>) => renderItem(row.item),
    [renderItem],
  );

  const commonProps = {
    keyExtractor,
    disableRightSwipe: true,
    rightOpenValue: -1 * DELETE_BTN_WIDTH,
    renderItem: renderItemCallback,
    renderHiddenItem: renderHiddenItemCallback,
    ListHeaderComponent,
    stickySectionHeadersEnabled: false,
  };

  return isSwipeableList(props) ? (
    <SwipeListView
      {...commonProps}
      data={props.items}
    />
  ) : (
    <SwipeListView
      {...commonProps}
      useSectionList
      sections={props.sections}
      renderSectionHeader={(info) => props.renderSectionHeader(info.section)}
    />
  );
};

export default SwipeableList;
