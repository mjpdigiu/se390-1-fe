import React, { useState, useEffect } from 'react';
import { Icon } from 'react-native-elements';
import {
  Text, View, StyleSheet, ViewStyle, TextInput,
} from 'react-native';
import theme from '../theme';

// Google Place Autocomplete types: https://developers.google.com/places/web-service/autocomplete
interface Prediction {
  description: string;
  place_id: string;
}
interface AutocompleteResponse {
  status: string;
  predictions: Prediction[];
}

const HEIGHT = 40;

const styles = StyleSheet.create({
  container: {
    width: '70%',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  searchBar: {
    backgroundColor: theme.colors.white,
    padding: theme.spacing.small,
    borderRadius: HEIGHT / 2,
    height: HEIGHT,
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropdown: {
    backgroundColor: theme.colors.white,
    marginTop: theme.spacing.small,
    borderRadius: 6,
    padding: theme.spacing.small,
    fontSize: 20,
  },
  searchResult: {
    padding: theme.spacing.small,
  },
});

interface Props {
  style?: ViewStyle;
}

const GOOGLE_API_KEY = 'AIzaSyB7ueZ5fnM1SwpWwoUi88H5k3_tkLIoz5I';

const GeoSearchBar: React.FC<Props> = ({ style }) => {
  const [text, setText] = useState<string>('');
  const [predictions, setPredictions] = useState<Prediction[]>([]);

  useEffect(() => {
    const fetchPredictions = async () => {
      const response = await fetch('https://maps.googleapis.com/maps/api/place/autocomplete/'
        + 'json?'
        + `input=${encodeURIComponent(text)}`
        + `&key=${encodeURIComponent(GOOGLE_API_KEY)}`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      const result: AutocompleteResponse = await response.json();
      if (result.status === 'OK') {
        console.log(result);
        setPredictions(result.predictions);
      } else {
        console.warn(`fetch predictions failed: ${result}`);
      }
    };

    if (text.length >= 1) {
      fetchPredictions();
    } else {
      setPredictions([]);
    }
  }, [text]);

  return (
    <View style={[style, { display: 'flex', alignItems: 'center' }]}>
      <View style={styles.container}>
        <View style={[styles.searchBar, styles.shadow]}>
          <Icon
            color={theme.colors.mediumgray}
            name={'search'}
            type={'font-awesome-5'}
            size={20}
            style={{ marginLeft: 4 }}
          />
          <TextInput
            value={text}
            onChangeText={setText}
            placeholder={'Search ...'}
            style={{ marginLeft: theme.spacing.small, fontSize: 20, flex: 1 }}
          />
        </View>
        {predictions.length > 0 && (
          <View style={[styles.dropdown, styles.shadow]}>
            {predictions.map((prediction) => (
              <View style={styles.searchResult} key={prediction.place_id}>
                <Text>{prediction.description}</Text>
              </View>
            ))}
          </View>
        )}
      </View>
    </View>
  );
};

export default GeoSearchBar;
