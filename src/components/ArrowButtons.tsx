import React, { useCallback } from 'react';
import { View, ViewStyle } from 'react-native';
import { Icon } from 'react-native-elements';
import { LatLng } from 'react-native-maps';
import theme from '../theme';

interface Props {
  onDirectionPress: (locationDelta: LatLng) => void;
  style?: ViewStyle;
}

// NOTE: intended to be a dev tool
// paste this JSX where you may need it:
/**
 * <ArrowButtons
    onDirectionPress={(locationDelta: LatLng) => {
      onLocationChange({
        latitude: userLocation!.latitude + locationDelta.latitude,
        longitude: userLocation!.longitude + locationDelta.longitude,
      });
    }}
    style={{
      position: 'absolute',
      bottom: 20,
      left: 20,
    }}
  />
 */

const ArrowButtons: React.FC<Props> = ({ onDirectionPress, style }) => {
  const onPress = useCallback((direction: string) => {
    const delta = 0.0001;
    const locationDelta: LatLng = { latitude: 0, longitude: 0 };
    switch (direction) {
      case 'up':
        locationDelta.latitude += delta;
        break;
      case 'right':
        locationDelta.longitude += delta;
        break;
      case 'left':
        locationDelta.longitude -= delta;
        break;
      case 'down':
        locationDelta.latitude -= delta;
        break;
      default:
        console.warn('bruh');
    }
    onDirectionPress(locationDelta);
  }, [onDirectionPress]);

  return (
    <View style={style}>
      {['up', 'left', 'right', 'down'].map((direction) => (
        <Icon
          key={direction}
          name={`arrow-${direction}`}
          type={'font-awesome-5'}
          reverse
          raised
          onPress={() => onPress(direction)}
          color={theme.colors.black}
          size={34}
        />
      ))}
    </View>
  );
};

export default ArrowButtons;
