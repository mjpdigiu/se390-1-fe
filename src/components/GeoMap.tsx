import React, {
  useEffect, useState, useRef, createContext, useContext, useCallback,
} from 'react';
import MapView, { LatLng } from 'react-native-maps';
import { useDispatch, useSelector } from 'react-redux';
import { watchPositionAsync } from 'expo-location';
import {
  StyleSheet, ViewStyle, View,
} from 'react-native';
import { WaveIndicator } from 'react-native-indicators';
import mapStyleDay from '../../map-styles/MapStyleDay.json';
import { geoSearch } from '../store/caches/CachesActions';
import { distanceBetweenCoordinates } from '../utils/Geography';
import { getIdToken } from '../store/auth/AuthSelectors';
import theme from '../theme';

const styles = StyleSheet.create({
  hideGoogleLogo: {
    ...StyleSheet.absoluteFillObject,
    bottom: -28,
  },
  lowCenter: {
    ...StyleSheet.absoluteFillObject,
    bottom: -100,
  },
  loadingOverlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: theme.colors.avocado,
    justifyContent: 'center',
    alignItems: 'center',
  },
});


interface UserLocationContext {
  userLocation: LatLng;
}

const initialState: UserLocationContext = {
  userLocation: { latitude: 0, longitude: 0 },
};
export const UserLocationContext = createContext(initialState);

/**
 * React Context provider
 * Subscribes to user location updates and passes `userLocation` into context
 * for child components to consume
 */
export const UserLocationProvider: React.FC = ({ children }) => {
  const [userLocation, setUserLocation] = useState<LatLng>({ latitude: 0, longitude: 0 });

  // Subscribe to user location updates
  useEffect(() => {
    let unsubscribe: () => void;

    const subscribeToUserLocation = async () => {
      const { remove } = await watchPositionAsync({
        accuracy: 5, // "Highest"
        distanceInterval: 5, // meters
      },
      ({ coords }) => setUserLocation(coords));
      unsubscribe = remove;
    };

    subscribeToUserLocation();

    // Unsubscribe as component cleans up
    // return () => unsubscribe();
  }, [setUserLocation]);

  return (
    <UserLocationContext.Provider value={{ userLocation }}>
      {children}
    </UserLocationContext.Provider>
  );
};

/**
 * Wraps GeoMap and geosearches when user moves far enough away
 * from last search position
 */
export const GeoMapSearch: React.FC = ({ children }) => {
  const dispatch = useDispatch();
  const { userLocation } = useContext(UserLocationContext);
  const idToken = useSelector(getIdToken);
  const [lastGeosearch, setLastGeosearch] = useState<LatLng>();

  // Call geosearch when user has moved 100m since last geosearch location
  useEffect(() => {
    if (!userLocation) return;
    if (!lastGeosearch) {
      dispatch(geoSearch(userLocation, '1', idToken));
      setLastGeosearch(userLocation);
      return;
    }
    const d = distanceBetweenCoordinates(lastGeosearch, userLocation);
    if (d > 0.100) {
      dispatch(geoSearch(userLocation, '1', idToken));
      setLastGeosearch(userLocation);
    }
  }, [userLocation, lastGeosearch, setLastGeosearch]);

  return (
    <GeoMap location={userLocation} style={styles.lowCenter}>
      {children}
    </GeoMap>
  );
};

/**
 * Wraps GeoMap and provides user location
 */
export const GeoMapFollow: React.FC = ({ children }) => {
  const { userLocation } = useContext(UserLocationContext);
  return (
    <GeoMap location={userLocation}>
      {children}
    </GeoMap>
  );
};

interface GeoMapProps {
  location: LatLng;
  children?: any;
  rotationEnabled?: boolean;
  style?: ViewStyle;
}

/**
 * Base map component that centers at `location` prop.
 * Animates to new location when prop is updated.
 * Optionally forwards a ref.
 */
const GeoMap = React.forwardRef<MapView, GeoMapProps>(({
  children,
  location,
  rotationEnabled = true,
  style,
}, ref?) => {
  const mapRef = useRef<MapView>(null);

  useEffect(() => {
    mapRef.current?.animateCamera({
      center: location,
    }, { duration: 100 });
  }, [location]);


  const [isMapLoading, setMapLoading] = useState<boolean>(true);
  const onMapReady = useCallback(() => {
    setTimeout(() => setMapLoading(false), 1500);
  }, [setMapLoading]);

  // Zoom in effect on first load
  useEffect(() => {
    if (!isMapLoading) {
      mapRef.current?.animateCamera({
        zoom: 18,
        pitch: 90,
      }, { duration: 500 });
    }
  }, [isMapLoading]);

  return (
    <View style={StyleSheet.absoluteFill}>
      <MapView
        ref={ref ?? mapRef}
        onMapReady={onMapReady}
        provider={'google'}
        style={style ?? styles.hideGoogleLogo}
        customMapStyle={mapStyleDay}
        showsBuildings={false}
        showsIndoors={false}
        showsPointsOfInterest={false}
        showsCompass
        initialCamera={{
          center: location,
          heading: 0,
          pitch: 0,
          zoom: 14,
          altitude: 0, // not used for Google Maps
        }}
        rotateEnabled={rotationEnabled}
        pitchEnabled={false}
        zoomEnabled={false}
        scrollEnabled={false}
      >
        {children}
      </MapView>
      {isMapLoading && (
        <View style={styles.loadingOverlay}>
          <WaveIndicator
            color={theme.colors.white}
            count={2}
            size={80}
            waveMode={'outline'}
          />
        </View>
      )}
    </View>
  );
});

export default GeoMap;
