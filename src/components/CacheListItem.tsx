import React, { useCallback, useMemo } from 'react';
import { Image, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ListItem, Icon } from 'react-native-elements';
import theme from '../theme';
import { NavParams } from '../screens/CacheDetailScreen';
import { CacheType } from '../store/caches/CachesState';
import { cacheTypeToIconName, cacheTypeToColorGradient } from '../utils/CacheUtils';
import GeoText from './GeoText';

const IMG_SIZE = 50;

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: theme.colors.white,
  },
  image: {
    width: IMG_SIZE,
    height: IMG_SIZE,
  },
});

interface Props {
  found: number;
  id: string;
  message: string;
  name: string;

  cacheType?: CacheType;
  yourMove?: boolean;
  foundReverseGeocode?: string;
  imageUri?: string;
}

const CacheListItem: React.FC<Props> = ({
  found,
  id,
  message,
  name,

  cacheType,
  yourMove,
  foundReverseGeocode,
  imageUri,
}) => {
  const { navigate } = useNavigation();
  const iconName = useMemo(() => cacheTypeToIconName(cacheType!), [cacheType]);
  const iconColor = useMemo(() => cacheTypeToColorGradient(cacheType!)[0], [cacheType]);

  const onPress = useCallback(() => navigate('CacheDetail', { id, title: `${name}'s Cache` } as NavParams), [navigate, id]);

  return (
    <ListItem
      style={styles.listItem}
      leftIcon={(
        <Icon
          name={iconName}
          type={'font-awesome-5'}
          color={iconColor}
          reverse
        />
      )}
      title={<GeoText variant={'textBold'}>{name}</GeoText>}
      subtitle={<GeoText variant={'secondaryText'}>{`@ ${foundReverseGeocode}`}</GeoText>}
      rightTitle={<GeoText variant={'textBold'} color={'blue'}>{yourMove ? 'YOUR MOVE' : undefined}</GeoText>}
      rightElement={imageUri ? (
        <Image
          style={styles.image}
          source={{ uri: imageUri }}
        />
      ) : undefined}
      onPress={onPress}
    />
  );
};
export default CacheListItem;
