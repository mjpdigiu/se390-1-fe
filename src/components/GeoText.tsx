import React from 'react';
import {
  Text, TextStyle, StyleSheet, TextProps,
} from 'react-native';
import { Color, COLOR_MAP } from '../theme';

type Variant =
| 'title'
| 'blurb'
| 'text'
| 'textBold'
| 'secondaryText'
| 'screenHeader'
| 'formHeader'
| 'button';

// These fonts are loaded in AppLoader
const fontFamilies = {
  // extraLight: 'Nunito_200ExtraLight',
  // light: 'Nunito_300Light',
  regular: 'Nunito_400Regular',
  semiBold: 'Nunito_600SemiBold',
  bold: 'Nunito_700Bold',
  extraBold: 'Nunito_800ExtraBold',
  // black: 'Nunito_900Black',
} as const;

export const textStyles = StyleSheet.create({
  title: {
    fontSize: 34,
    fontFamily: fontFamilies.bold,
  },
  blurb: {
    fontSize: 21,
    fontFamily: fontFamilies.semiBold,
  },
  text: {
    fontSize: 17,
    fontFamily: fontFamilies.regular,
  },
  textBold: {
    fontSize: 17,
    fontFamily: fontFamilies.bold,
  },
  secondaryText: {
    fontSize: 15,
    fontFamily: fontFamilies.regular,
    lineHeight: 18,
  },
  screenHeader: {
    fontSize: 17,
    fontFamily: fontFamilies.extraBold,
  },
  formHeader: {
    fontSize: 21,
    fontFamily: fontFamilies.bold,
    textTransform: 'uppercase',
  },
  button: {
    fontSize: 17,
    fontFamily: fontFamilies.semiBold,
  },
});

interface Props {
  align?: 'left' | 'center' | 'right';
  variant?: Variant;
  color?: Color;
  style?: TextStyle;
}

const GeoText: React.FC<Props & TextProps> = ({
  align = 'left',
  children,
  variant = 'text',
  color = 'black',
  style,
  ...textProps
}) => (
  <Text
    style={[
      textStyles[variant],
      style,
      {
        color: COLOR_MAP[color],
        textAlign: align,
      },
    ]}
    {...textProps}
  >
    {children}
  </Text>
);

export default GeoText;
