import React, { useState, useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';
import theme from '../theme';
import GeoText from './GeoText';
import { Cache } from '../store/caches/CachesState';
import GeoButton from './GeoButton';
import { GeoMarkerBubble } from './GeoMarker';
import TicTacToe, { SAMPLE_TICTACTOE_STATE, TicTacToeBoardState } from '../screens/create/TicTacToe';

const styles = StyleSheet.create({
  modal: {
    marginHorizontal: theme.spacing.medium,
    marginVertical: 0,
  },
  container: {
    height: '80%',
    backgroundColor: theme.colors.white,
    justifyContent: 'space-between',

    borderRadius: theme.spacing.large,
    // borderWidth: 4,
    // borderColor: theme.colors.mediumgray,
    // borderStyle: 'solid',
  },
  bubble: {
    position: 'absolute',
    top: -50,
    left: 20,
  },
});

interface Props {
  show: boolean;
  cache: Cache;
  onClose: () => void;
  onSave: () => void;
  onReport: () => void;
}

const CacheFoundModal: React.FC<Props> = ({
  show,
  cache,
  onClose,
  onSave,
  onReport,
}) => {
  if (!cache) {
    return null;
  }

  // TODO: move cache type contents into seperate components
  const [moveMade, setMoveMade] = useState<boolean>(false);
  const [ticTacToeState, setTicTacToeState] = useState<TicTacToeBoardState>(SAMPLE_TICTACTOE_STATE);
  const onCellPress = useCallback((row: number, col: number) => {
    setMoveMade(true);
    const temp = [...SAMPLE_TICTACTOE_STATE] as TicTacToeBoardState;
    const tempRow = [...temp[row]];
    tempRow[col] = 'X';
    temp[row] = tempRow;

    setTicTacToeState(temp);
  }, [setTicTacToeState, setMoveMade]);

  return (
    <Modal
      isVisible={show}
      onSwipeComplete={onClose}
      swipeDirection={['up', 'left', 'right', 'down']}
      style={styles.modal}
      backdropOpacity={0.25}
      onBackdropPress={onClose}
    >
      <View style={styles.container}>
        <GeoMarkerBubble
          cacheType={cache.type!}
          style={styles.bubble}
        />

        <View style={{ alignItems: 'center', flex: 1 }}>
          <GeoText
            variant={'title'}
            style={{
              marginTop: theme.spacing.xlarge,
              marginBottom: theme.spacing.medium,
            }}
          >
            {cache.name!}
          </GeoText>

          {cache.type === 'Game' && (
            <TicTacToe boardState={ticTacToeState} onCellPress={onCellPress} />
          )}
          {cache.type === 'Message' && (
            <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: theme.spacing.screenPadding }}>
              <GeoText align={'center'}>
                {cache.message}
              </GeoText>
            </View>
          )}
        </View>

        <View style={{
          marginHorizontal: theme.spacing.screenPadding,
          marginVertical: theme.spacing.medium,
        }}
        >
          {cache.type === 'Game' && (
            <GeoButton
              title={'Make Move'}
              onPress={onSave}
              disabled={!moveMade}
            />
          )}
          {cache.type !== 'Game' && (
            <GeoButton
              title={'Save'}
              onPress={onSave}
              icon={'book'}
            />
          )}
        </View>
      </View>
    </Modal>
  );
};

export default CacheFoundModal;
