import React from 'react';
import {
  View, StyleSheet, ViewStyle, TouchableOpacity, Text,
} from 'react-native';
import { Icon } from 'react-native-elements';
import theme from '../theme';
import GeoText from './GeoText';

export const TAG_TYPES = [
  'artsy',
  'basic',
  'creative',
  'drawing',
  'epic',
  'funny',
  'gamer',
  'hacker',
  'italy',
  'jackbox',
  'karate',
  'love',
  'mdg',
] as const;
type Tag = typeof TAG_TYPES[number];

type ColorMap = {
  [key in Tag]: string;
}
const COLOR_MAP: ColorMap = {
  artsy: '#91F9E5',
  basic: '#76F7BF',
  creative: '#5FDD9D',
  drawing: '#EF6461',
  epic: '#2D82B7',
  funny: '#DEA54B',
  gamer: '#F2CD5D',
  hacker: '#D741A7',
  italy: '#9EADC8',
  jackbox: '#E4B363',
  karate: '#FFC759',
  love: '#FF7B9C',
  mdg: '#AD2831',
} as const;

const styles = StyleSheet.create({
  tag: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-start', // hack (?) to prevent full-width
    paddingHorizontal: theme.spacing.medium,
  },
  small: {
    height: 30,
    borderRadius: 30,
  },
  large: {
    height: 40,
    borderRadius: 40,
  },
  addTagButton: {
    backgroundColor: theme.colors.white,
    borderColor: theme.colors.mediumgray,
    borderWidth: 2,
    borderStyle: 'solid',
  },
});

interface AddTagButtonProps {
  onAdd: () => void;
  style?: ViewStyle;
}

export const AddTagButton: React.FC<AddTagButtonProps> = ({ onAdd, style }) => (
  <TouchableOpacity onPress={onAdd} style={style}>
    <View style={[styles.tag, styles.addTagButton, styles.small]}>
      <GeoText color={'mediumgray'} variant={'secondaryText'}>
        Add tag
      </GeoText>
    </View>
  </TouchableOpacity>
);

interface Props {
  tag: Tag;
  onRemove?: () => void;
  size?: 'small' | 'large';
  style?: ViewStyle;
}

const Tag: React.FC<Props> = ({
  tag,
  onRemove,
  size = 'small',
  style,
}) => (
  <View style={[style, styles.tag, styles[size], { backgroundColor: COLOR_MAP[tag] }]}>
    <GeoText color={'white'} variant={size === 'small' ? 'text' : 'blurb'}>{tag}</GeoText>
    { onRemove && (
      <Icon
        color={theme.colors.white}
        name={'times'}
        type={'font-awesome-5'}
        size={12}
        style={{ padding: theme.spacing.small, paddingRight: 0 }}
        onPress={onRemove}
      />
    )}
  </View>
);

export default Tag;
