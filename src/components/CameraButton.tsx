import React from 'react';
import {
  TouchableOpacity, View, ViewStyle, StyleSheet,
} from 'react-native';
import theme from '../theme';

const SIZE = 50;

const styles = StyleSheet.create({
  outerRing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    borderWidth: 2,
    height: SIZE,
    width: SIZE,
    borderRadius: SIZE,
    borderColor: theme.colors.white,
  },
  innerCircle: {
    borderWidth: 2,
    height: SIZE - 10,
    width: SIZE - 10,
    borderRadius: SIZE - 10,
    borderColor: theme.colors.white,
    backgroundColor: theme.colors.white,
  },
});

interface Props {
  onPress: () => void;
  style?: ViewStyle;
}

const CameraButton: React.FC<Props> = ({ onPress, style }) => (
  <TouchableOpacity style={style} onPress={onPress}>
    <View style={styles.outerRing}>
      <View style={styles.innerCircle} />
    </View>
  </TouchableOpacity>
);

export default CameraButton;
