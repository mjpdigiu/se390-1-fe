import { Group } from './store/groups/GroupState';

export const sampleGroups: Group[] = [
  {
    groupId: '0',
    groupName: 'School Squad',
    memberList: [
      {
        userId: 'will.brown',
        permissions: { admin: true },
      },
      {
        userId: 'jayson',
        permissions: { admin: false },
      },
      {
        userId: 'krisztian',
        permissions: { admin: false },
      },
    ],
    cacheIdList: [
      '1',
      '2',
    ],
  },
  {
    groupId: '1',
    groupName: 'Cool Club',
    memberList: [
      {
        userId: 'will.brown',
        permissions: { admin: true },
      },
      {
        userId: 'krisztian',
        permissions: { admin: false },
      },
      {
        userId: 'jason',
        permissions: { admin: false },
      },
      {
        userId: 'milan.digiuseppe',
        permissions: { admin: false },
      },
      {
        userId: 'roxy.the.dog',
        permissions: { admin: false },
      },
      {
        userId: 'clifford.the.big.dog',
        permissions: { admin: false },
      },
      {
        userId: 'dogmeat',
        permissions: { admin: false },
      },
      {
        userId: 'epic.gamer.cool.dude',
        permissions: { admin: false },
      },
    ],
    cacheIdList: [
      '3',
    ],
  },
];
