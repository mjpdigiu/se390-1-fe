import React from 'react';
import { Icon } from 'react-native-elements';
import theme from '../theme';

interface Props {
  type?: 'back' | 'close';
  onPress: () => void;
}

const HeaderLeftButton: React.FC<Props> = ({ type = 'back', onPress }) => (
  <Icon
    name={type === 'back' ? 'chevron-left' : 'times'}
    type={'font-awesome-5'}
    color={theme.colors.white}
    onPress={onPress}
    style={{ paddingHorizontal: theme.spacing.screenPadding }}
  />
);

export default HeaderLeftButton;
