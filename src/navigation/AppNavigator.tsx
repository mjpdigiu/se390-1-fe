import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';
import CreateCacheNavigator from './CreateCacheNavigator';
import CameraModal from '../screens/create/CameraModal';
import TagScreen from '../screens/TagScreen';
import HomeNavigator from './HomeNavigator';
import HeaderLeftButton from './HeaderLeftButton';
import theme from '../theme';
import EditProfileModal from '../screens/EditProfileModal';
import OnboardingScreen from '../screens/OnboardingScreen';
import { textStyles } from '../components/GeoText';
import { getHasBeenOnboarded } from '../store/preferences/PreferencesSelectors';

const Stack = createStackNavigator();
const AppNavigator = () => {
  const hasBeenOnboarded = useSelector(getHasBeenOnboarded);

  return (
    <Stack.Navigator
      mode={'modal'}
      screenOptions={({ navigation }) => ({
        headerLeft: () => <HeaderLeftButton onPress={() => navigation.goBack()} type={'close'} />,
        headerStyle: { backgroundColor: theme.colors.green },
        headerTintColor: theme.colors.white,
        headerTitleStyle: textStyles.screenHeader,
      })}
    >
      {/*
        Some implicit navigation is happening here:
        The navigator will render the first screen below by default,
        so the OnboardingScreen will appear conditionally before the
        HomeNavigator
      */}
      {!hasBeenOnboarded && (
        <Stack.Screen
          name={'Onboarding'}
          component={OnboardingScreen}
          options={{ headerShown: false }}
        />
      )}
      <Stack.Screen
        name={'Home'}
        component={HomeNavigator}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'Create'}
        component={CreateCacheNavigator}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'CameraModal'}
        component={CameraModal}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'TagScreen'}
        component={TagScreen}
        options={{ title: 'Tags' }}
      />
      <Stack.Screen
        name={'EditProfile'}
        component={EditProfileModal}
        options={{ title: 'Edit Profile' }}
      />
    </Stack.Navigator>
  );
};

export default AppNavigator;
