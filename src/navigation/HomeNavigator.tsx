import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import DiscoverScreen from '../screens/DiscoverScreen';
import SettingsScreen from '../screens/SettingsScreen';
import HeaderLeftButton from './HeaderLeftButton';
import theme from '../theme';
import SavedListScreen from '../screens/SavedListScreen';
import CacheDetailScreen, { NavParams } from '../screens/CacheDetailScreen';
import RevisitCacheScreen from '../screens/RevisitCacheScreen';
import DevScreen from '../screens/DevScreen';
import GroupScreen from '../screens/GroupScreen';
import ProfileScreen from '../screens/ProfileScreen';
import { textStyles } from '../components/GeoText';

const Stack = createStackNavigator();
const HomeNavigator = () => (
  <Stack.Navigator
    screenOptions={({ navigation }) => ({
      headerLeft: () => <HeaderLeftButton onPress={() => navigation.goBack()} />,
      headerStyle: { backgroundColor: theme.colors.green },
      headerTintColor: theme.colors.white,
      headerTitleStyle: textStyles.screenHeader,
    })}
  >
    <Stack.Screen
      name={'Discover'}
      component={DiscoverScreen}
      options={{ headerShown: false }}
    />

    <Stack.Screen
      name={'SavedList'}
      component={SavedListScreen}
      options={{ title: 'Saved' }}
    />
    <Stack.Screen
      name={'CacheDetail'}
      component={CacheDetailScreen}
      options={({ route }) => ({ title: (route.params as NavParams).title })}
    />
    <Stack.Screen
      name={'RevisitCache'}
      component={RevisitCacheScreen}
      options={({ route }) => ({ title: (route.params as NavParams).title })}
    />
    <Stack.Screen
      name={'Profile'}
      component={ProfileScreen}
      options={{ title: 'Profile' }}
    />
    <Stack.Screen
      name={'Settings'}
      component={SettingsScreen}
      options={{ title: 'Settings' }}
    />
    <Stack.Screen
      name={'Dev'}
      component={DevScreen}
    />
    <Stack.Screen
      name={'Group'}
      component={GroupScreen}
    />
  </Stack.Navigator>
);

export default HomeNavigator;
