import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import CacheMessageScreen from '../screens/create/CacheMessageScreen';
import CacheDurationScreen from '../screens/create/CacheDurationScreen';
import CacheSubmitScreen from '../screens/create/CacheSubmitScreen';
import theme from '../theme';
import CacheTypeScreen from '../screens/create/CacheTypeScreen';
import GamesScreen from '../screens/create/GamesScreen';
import OutdoorsScreen from '../screens/create/OutdoorsScreen';
import HeaderLeftButton from './HeaderLeftButton';
import PhotographyScreen from '../screens/create/PhotographyScreen';
import { textStyles } from '../components/GeoText';

const Stack = createStackNavigator();
const CreateCacheNavigator = () => (
  <Stack.Navigator
    screenOptions={
      ({ navigation, route }) => ({
        headerStyle: { backgroundColor: theme.colors.green },
        headerTintColor: theme.colors.white,
        headerTitleStyle: textStyles.screenHeader,
        headerLeft: () => (
          <HeaderLeftButton
            type={route.name === 'CacheType' ? 'close' : 'back'}
            onPress={() => navigation.goBack()}
          />
        ),
      })
    }
  >
    <Stack.Screen
      name={'CacheType'}
      component={CacheTypeScreen}
      options={{ title: 'Choose a Type' }}
    />
    <Stack.Screen
      name={'GamesScreen'}
      component={GamesScreen}
      options={{ title: 'Games' }}
    />
    <Stack.Screen
      name={'OutdoorsScreen'}
      component={OutdoorsScreen}
      options={{ title: 'Outdoors' }}
    />
    <Stack.Screen
      name={'PhotographyScreen'}
      component={PhotographyScreen}
      options={{ title: 'Photography' }}
    />
    <Stack.Screen
      name={'CacheContent'}
      component={CacheMessageScreen}
      options={{ title: 'Message' }}
    />
    <Stack.Screen
      name={'CacheDuration'}
      component={CacheDurationScreen}
      options={{ title: 'Hide Time' }}
    />
    <Stack.Screen
      name={'CacheSubmit'}
      component={CacheSubmitScreen}
      options={{ title: 'New Cache' }}
    />
  </Stack.Navigator>
);

export default CreateCacheNavigator;
