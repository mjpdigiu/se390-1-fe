import React from 'react';
import { useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from '../screens/SignInScreen';
import { getIdToken } from '../store/auth/AuthSelectors';
import AppNavigator from './AppNavigator';

const Stack = createStackNavigator();
const RootNavigator = () => {
  const token = useSelector(getIdToken);

  return (
    <Stack.Navigator headerMode={'none'}>
      {token ? (
        <Stack.Screen
          name={'AppNavigator'}
          component={AppNavigator}
        />
      ) : (
        <Stack.Screen
          name={'SignIn'}
          component={SignInScreen}
        />
      )}
    </Stack.Navigator>
  );
};

export default RootNavigator;
